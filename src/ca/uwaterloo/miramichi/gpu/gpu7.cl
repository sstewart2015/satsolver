// Macros for literals
#define LIT_SIGN(p) ((p) & 1)
#define LIT_VAR(p) ((p) >> 1)

// Lifted Boolean domain
#define LBOOL_TRUE 0
#define LBOOL_FALSE 1
#define LBOOL_UNDEF 2

// Statistics
#define STAT_BLOCK_SIZE 5
#define STAT_OFFSET_SAT 0
#define STAT_OFFSET_UNIT 1
#define STAT_OFFSET_CONFL 2
#define STAT_OFFSET_UNRES 3
#define STAT_OFFSET_WASTE 4

// Inspect one literal for one explorer
__kernel void gpu7_loop(
    // inputs
    const int num_explorers,  // unused
    const int num_variables,  // unused, replaced by pre-processor constant
    const int num_literals,
    const int num_clauses,
    __global const char* const g_assignments,     // size == num_explorers * num_variables
    __global const uint*  const literals,          // size == sum of length of all clauses
    //__global const int*  const lit2clause,        // size == same as literals[]
    __global const int*  const clause_lengths,    // size == number of clauses
    // from the pre-processor:
    // MULTI_FACTOR 
    // UNROLL 
    // WORKGROUPSIZE
    // LITS_PER_WORKGROUP = WORKGROUPSIZE * UNROLL
    // NUM_VARIABLES
    // NUM_VARIABLES_ALIGNED
    // ASSIGNMENT_CELLS
    // ASSIGN_LOOP_BOUND_FLOOR
    // ASSIGN_LOOP_BOUND_CEIL

    // outputs
    // initialized to zeroes
    __global int* const units,      // size == num_explorers * MULTI_FACTOR
    __global int* const conflicts,  // size == num_explorers * MULTI_FACTOR
    // initialized to zeroes
    __global int* const stats       // size == num_explorer * STAT_BLOCK_SIZE

    ) {

  // decode work item
  // first dimension is explorer
  const int explorerID = get_global_id(0);
  // second dimension is literals
  const int groupID = get_group_id(1);
  const int threadID = get_local_id(1);
  const int literalID = get_global_id(1);


  // pre-loop to coalesce reads from assignments
  __local char assignments[ASSIGNMENT_CELLS];
  const int g_assignment_offset = ASSIGNMENT_CELLS * explorerID;
  //const int ASSIGN_LOOP_BOUND_FLOOR = (int) floor( (float) ASSIGNMENT_CELLS / (float) WORKGROUPSIZE );
  //const int ASSIGN_LOOP_BOUND_CEIL = (int) ceil( (float) ASSIGNMENT_CELLS / (float) WORKGROUPSIZE );
  // split the loop in two: first where we know every iteration has data; second where we need to check bounds
  // for these iterations we know that we are in bounds
  // why is this unroll pragma rejected by the compiler?
  //pragma unroll ASSIGN_LOOP_BOUND_FLOOR
  for (int loop=0, cell=threadID; loop < ASSIGN_LOOP_BOUND_FLOOR; loop++, cell += WORKGROUPSIZE) {
    assignments[cell] = g_assignments[g_assignment_offset + cell];
  }
  // for these iterations we need to check that we are in bounds
  // there will be at most one iteration of this loop
  #pragma unroll 1
  for (int loop=ASSIGN_LOOP_BOUND_FLOOR, cell= WORKGROUPSIZE * ASSIGN_LOOP_BOUND_FLOOR + threadID; loop < ASSIGN_LOOP_BOUND_CEIL; loop++, cell += WORKGROUPSIZE) {
    if (cell < ASSIGNMENT_CELLS) {
      assignments[cell] = g_assignments[g_assignment_offset + cell];
    }
  }


  // the main loop starts here

  // one cell for each clause being processed in this workgroup
  // the maximum number of clauses that could be processed is the number of literals

  // clause IDs to remember
  int unitClauseID = -1;
  int conflictClauseID = -1;

  // the first literal in this workgroup and thread
  const int group_base = groupID * LITS_PER_WORKGROUP;
  const int thread_base = group_base + threadID;

  __local int num_lit_undefs_local[LITS_PER_WORKGROUP];

  #pragma unroll UNROLL
  for (int loop=0, literalID = thread_base; loop < UNROLL; loop++, literalID += WORKGROUPSIZE) {
  
    // literalID might be greater than num_literals due to workgroup sizing
    const int inWorkgroup = (literalID < num_literals);

    if (inWorkgroup) {
      // decode clauseID
      const uint litcell = literals[literalID];
      const int p = litcell & 0xFFFF;     // literals is the low bits
      const int clauseID = (litcell >> 16); // clauseID is the high bits
      // const int clauseID = lit2clause[literalID];

      const int localClauseID = clauseID % LITS_PER_WORKGROUP;
      // initialize num_lit_undefs_local
      const int c_length = clause_lengths[clauseID];
      num_lit_undefs_local[localClauseID] = c_length;
   

      // decode assignment
      //const int p = literals[literalID];
      const int psign = LIT_SIGN(p);
      
      const int offset = 0; //NUM_VARIABLES_ALIGNED * explorerID;
      const int var_offset = offset + LIT_VAR(p);
      const int remainder = (var_offset & 0x3) << 1;
      const int cell = var_offset >> 2;
      const uchar assignment_loc = assignments[cell];

      const uchar assignment = (assignment_loc >> remainder) & 0x3;

      // decode the three cases: true, false, undefined
      const int a_true = (assignment==psign);
      const int a_undef = (assignment==LBOOL_UNDEF);
      const int a_false = !a_undef && !a_true;

      // select(fv, tv, c) means: c ? tv : fv;
      // adjust num_lit_undefs_local according to the three cases
      int subtrahend = 0;                                       // do nothing
      subtrahend = select(subtrahend, 1, a_false);              // decrement
      subtrahend = select(subtrahend, WORKGROUPSIZE, a_true);   // drive negative
      atomic_sub(&num_lit_undefs_local[localClauseID], subtrahend);

      // have we found an interesting result?
      const int undef = num_lit_undefs_local[localClauseID];

      // if undef==0 it is definitely a conflict clause
      conflictClauseID = select(conflictClauseID, clauseID, undef==0);
      // if undef==1 it might be a unit clause
      // later undef might drop to zero
      // when conflicts/units are merged on the CPU the conflicts will take priority
      unitClauseID = select(unitClauseID, clauseID, undef==1);
    } // end if
  
  } // end for
 
  // upload if interesting
  if (conflictClauseID >= 0) {
    const int cid_ndx = (explorerID * MULTI_FACTOR) + conflictClauseID % MULTI_FACTOR;
    conflicts[cid_ndx] = conflictClauseID + 1;

  } else if (unitClauseID >= 0) {
    const int cid_ndx = (explorerID * MULTI_FACTOR) + unitClauseID % MULTI_FACTOR;
    units[cid_ndx] = -unitClauseID -1;
  } // end if

} // end proc
