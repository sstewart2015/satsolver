package ca.uwaterloo.miramichi.gpu;

import java.util.logging.Logger;

/**
 * Instructions and storage for an OpenCLComputer to act on.
 */
public interface OpenCLComputation {

  public Mem[] memories();

  public Kernel[] kernels();

  public void uploadPreprocessing();

  public void uploadPostprocessing();

  public void downloadPreprocessing();

  public void downloadPostprocessing();

  public Logger logger();

}
