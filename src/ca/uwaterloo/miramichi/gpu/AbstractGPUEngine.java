package ca.uwaterloo.miramichi.gpu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ca.uwaterloo.miramichi.Assignment;
import ca.uwaterloo.miramichi.Clause;
import ca.uwaterloo.miramichi.ClauseDB;
import ca.uwaterloo.miramichi.Conductor;
import ca.uwaterloo.miramichi.Constants;
import ca.uwaterloo.miramichi.Explorer;
import ca.uwaterloo.miramichi.Literal;
import ca.uwaterloo.miramichi.LogHelper;
import ca.uwaterloo.miramichi.QResult;
import ca.uwaterloo.miramichi.QueryEngine;
import ca.uwaterloo.miramichi.Util;

public abstract class AbstractGPUEngine extends QueryEngine implements OpenCLComputation {

  /** This flag will be mutated by Main. */
  public static boolean GPU_PROFILING = true;

  /** How many multi-propagations/conflicts to do. Will be set by Main. */
  public static int MULTI_FACTOR = -1;

  /**
   * Whether all pages should be computed, or only ones on which variables have changed. This will
   * be set by Main from the command line.
   */
  public static Boolean activePagesOnly = false;

  /** A shorter name for local usage. */
  protected final boolean ASSERTIONS = Constants.areAssertionsEnabled();

  /** The maximum number of explorers this GPUEngine can handle. */
  protected final int NUM_EXPLORERS;

  /** The maximum number of literals this GPUEngine can handle. */
  protected final int MAX_NUM_LITERALS;

  /** The maximum number of clauses this GPUEngine can handle. */
  protected final int MAX_NUM_CLAUSES;

  /** Sub-classes should check that this number matches that associated with their mems. */
  protected int num_clauses_consolidated = 0;

  /** Sub-classes should check that this number matches that associated with their mems. */
  protected int num_literals_consolidated = 0;

  /**
   * Accumulated count of the number of pages that were necessary to recompute, because one of their
   * variables had been changed. Each page would result in a number of workgroups equal to the
   * number of explorers.
   */
  protected int necessaryPageCount = 0;

  /**
   * Accumulated count of the number of page that were wasted computation because none of their
   * variables had changed. Each page would result in a number of workgroups equal to the number of
   * explorers.
   */
  protected int wastePageCount = 0;

  /** Assignments of all explorers. */
  protected final MemByte assignments = new MemByte("assignments", MemConfig.UploadOnly,
      new MemCapacity(-1, () -> Assignment.cellCount(numExplorers(), DB.varCount())));

  /** Which pages need to be recomputed in this cycle. */
  protected final MemInt pageMap = new MemInt("pageMap", MemConfig.UploadOnly, new MemCapacity(-1,
      () -> MAX_NUM_LITERALS / DB.workgroupSize + 1));

  /** So we can use this value on host after it has been uploaded. */
  protected int[] pageMapAlias = null;

  /** Number of active pages in this cycle. */
  protected int pageCounter = 0;

  /**
   * Holds an encoded clause id (id of unit or conflicting clause) for each explorer. Also encoded
   * in this value is the result type (<0 = unit, >0 = conflict, 0 = done)
   */
  protected final MemInt cid = new MemInt("cid", MemConfig.DownloadOnly, new MemCapacity(
      () -> numExplorers() * AbstractGPUEngine.MULTI_FACTOR));

  /** Statistics of clause inspection for each explorer. */
  protected final MemInt stats = !ASSERTIONS ? MemInt.EMPTY_MEM_INT() : new MemInt("stats",
      MemConfig.DownloadOnly, new MemCapacity(() -> numExplorers() * Constants.STAT_BLOCK_SIZE));

  protected final OpenCLComputer computer;

  public AbstractGPUEngine(final ClauseDB DB, final int num_explorers) {
    super(DB);
    this.NUM_EXPLORERS = num_explorers;
    this.MAX_NUM_CLAUSES = DB.numClauses() * Constants.CLAUSE_GROWTH_FACTOR;
    this.MAX_NUM_LITERALS = DB.numLiterals() * Constants.LITERAL_GROWTH_FACTOR;
    this.computer = new OpenCLComputer(GPU_PROFILING, ASSERTIONS, this);
    // computer.initialize() will be called by the sub-class constructors
  }

  protected int numExplorers() {
    return NUM_EXPLORERS;
  }

  /**
   * Synchronizes the engine's copy of the database with the that of the global, host memory-based
   * clause database, which is accessible via an object named DB. This procedure requires encoding
   * the new clauses into the proper format for this engine's clause database (cdb).
   *
   * @param learntClauses The clauses that were recently learnt by the DB. If null, then loads the
   *        entire DB.
   * @see ClauseDB#consolidate()
   * @see Conductor#run()
   */
  @Override
  protected void consolidateDBInner(final List<Clause> learntClauses) {
    // load the entire database? or just new clauses?
    // note that the DB will include the learntClauses,
    // so if we load the entire DB we will get them too
    final boolean load_entire = (num_clauses_consolidated == 0);
    // determine number of clauses
    final int num_clauses = load_entire ? DB.numClauses() : learntClauses.size();

    // determine number of literals
    final int num_literals;
    if (load_entire) {
      num_literals = DB.numLiterals();
    } else {
      int n = 0;
      for (final Clause c : learntClauses) {
        n += c.size();
      }
      num_literals = n;
      assert num_literals_consolidated == DB.numLiterals() - num_literals : "loaded_literals_count, DB.numLiterals(), num_literals == "
          + num_literals_consolidated + ", " + DB.numLiterals() + ", " + num_literals;
      assert num_clauses_consolidated == DB.numClauses() - num_clauses;
      // loaded_literals_count and loaded_clauses_count are updated later,
      // when these new data are actually copied to the GPU
      // for now they still hold the values of what is actually on the GPU
    }
    // allocate buffers to be loaded onto GPU
    final int[] literals = new int[num_literals];
    final int[] lit2clause = new int[num_literals];
    final int[] offsets = new int[num_clauses];
    final int[] lengths = new int[num_clauses];

    // populate buffers
    final int offset_baseline = load_entire ? 0 : num_literals_consolidated;
    final int clause_offset = DB.numClauses() - num_clauses;
    for (int i = 0, z = 0; i < num_clauses; i++) {
      final Clause cl = load_entire ? DB.lookup(i) : learntClauses.get(i);
      offsets[i] = z + offset_baseline;
      lengths[i] = cl.size();
      for (int j = 0; j < cl.size(); j++, z++) {
        literals[z] = cl.lit(j);
        lit2clause[z] = i + clause_offset;
      }
    }

    num_literals_consolidated += num_literals;
    num_clauses_consolidated += num_clauses;

    consolidateToMems(literals, lit2clause, offsets, lengths);
  }

  /**
   * This method releases OpenCL-related resources. These include memory objects (variables with the
   * d_ prefix), and OpenCL-related data structures (command queue, kernel, program, and context).
   */
  @Override
  protected void releaseResourcesInner() {
    computer.release();
    final LogHelper lh = new LogHelper(logger(), Constants.LOG_WIDTH);
    if (activePagesOnly) {
      lh.info("pages computed", necessaryPageCount);
      lh.info("pages ignored", wastePageCount);
    } else {
      lh.info("pages required", necessaryPageCount);
      lh.info("pages wasted", wastePageCount);
    }
  }

  /**
   * Computes the queue of queries by packaging them up for GPU processing.
   *
   * @param explorers a queue of queries to be processed by the GPU
   */
  @Override
  protected final void computeInner(final List<Explorer> explorers) {

    // check if explorers are contiguous, starting at 0
    explorers.sort(Explorer.EID_COMPARATOR);
    final int c = explorers.size() - 1;
    if (explorers.get(0).eid == 0 && explorers.get(c).eid == c) {
      // explorers are contiguous, starting from zero
      // we can avoid CPU-CPU memory copy of assignments
      // TODO: make access to the shared assignment array less ugly
      final Assignment assigns = explorers.get(0).assigns;
      assignments.h = assigns.a;
      // see which pages have updates that need to be processed
      final ArrayList<Integer> pagesToRecompute = new ArrayList<>();
      assert !assigns.getDirtyVars().isEmpty() : "some var must have been set";
      for (int i = 0; i < DB.pageCount(); i++) {
        if (DB.pageNeedsRecomputing(i, assigns)) {
          necessaryPageCount++;
          pagesToRecompute.add(i);
        } else {
          wastePageCount++;
        }
      }
      pageMapAlias = new int[pagesToRecompute.size()];
      for (int i = 0; i < pageMapAlias.length; i++) {
        pageMapAlias[i] = pagesToRecompute.get(i);
      }
      pageMap.h = pageMapAlias;
      // check pageMap
      if (Constants.areAssertionsEnabled()) {
        // must have at least one page to compute
        assert pageMap.h.length > 0 : "pageMap must have non-zero length";
        // must increase monotonically
        int lastPage = -1;
        for (int i = 0; i < pageMap.h.length; i++) {
          final int thisPage = pageMap.h[i];
          assert thisPage > lastPage : "pageMap must increase monotonically "
              + Util.i2s(thisPage, lastPage);
          lastPage = thisPage;
        }
      }
      assigns.clearDirtyVars();
      // are we actually going to follow the pageMap?
      if (!activePagesOnly) {
        // no, throw it out
        pageMapAlias = new int[DB.pageCount()];
        for (int i = 0; i < pageMapAlias.length; i++) {
          pageMapAlias[i] = i;
        }
        pageMap.h = pageMapAlias;
      }
    } else {
      // we need to copy the assignments bit-by-bit
      throw new UnsupportedOperationException(
          "the code here pre-dated the move to dense assignment array and so would no longer work");
    }

    // launch kernel (which also uploads and downloads the data)
    computer.compute();

    // communicate results back to explorers
    for (final Explorer q : explorers) {
      final int explorer = q.eid;

      // check that the totals from the per_clause kernel add up
      final int stat_offset = explorer * Constants.STAT_BLOCK_SIZE;
      final int totalSat = stats.h[stat_offset + Constants.STAT_OFFSET_SAT];
      final int totalUnres = stats.h[stat_offset + Constants.STAT_OFFSET_UNRES];
      final int totalUnit = stats.h[stat_offset + Constants.STAT_OFFSET_UNIT];
      final int totalConfl = stats.h[stat_offset + Constants.STAT_OFFSET_CONFL];
      final int totalWaste = stats.h[stat_offset + Constants.STAT_OFFSET_WASTE];

      final int totalStats = totalSat + totalUnres + totalUnit + totalConfl + totalWaste;
      if (shouldCheckTotalStats()) {
        assert totalStats == num_clauses_consolidated : "totalStats don't add up: "
            + Util.i2s(num_clauses_consolidated, totalStats, totalSat, totalUnres, totalUnit,
                totalConfl, totalWaste);
      }

      // decode results
      // lists for all explorers
      final List<Integer> unitClauseIDs = new ArrayList<>(cid.h_length());
      final List<Integer> conflictClauseIDs = new ArrayList<>(cid.h_length());
      final int cid_offset = explorer * AbstractGPUEngine.MULTI_FACTOR;
      for (int j = 0; j < AbstractGPUEngine.MULTI_FACTOR; j++) {
        final int cj = cid.h[cid_offset + j];
        if (cj < 0) {
          // unit clause
          unitClauseIDs.add((cj + 1) * -1);
        } else if (cj > 0) {
          // conflict clause
          conflictClauseIDs.add(cj - 1);
        }
      }

      // construct QResult
      final List<Integer> clauseIds;
      final int[] unitLits;
      final QResult.Type result_type;
      if (!conflictClauseIDs.isEmpty()) {
        // conflict
        result_type = QResult.Type.CONFLICT;
        clauseIds = conflictClauseIDs;
        unitLits = null;
      } else if (!unitClauseIDs.isEmpty()) {
        // unit
        result_type = QResult.Type.UNIT;
        clauseIds = unitClauseIDs;
        // determine unit literals for each unit clause
        Integer lit = null;
        unitLits = new int[clauseIds.size()];
        for (int i = 0; i < clauseIds.size(); i++) {
          final Clause cl = DB.lookup(clauseIds.get(i));
          final Assignment qAssignments = q.assigns;
          for (int j = 0; j < cl.size(); j++) {
            final int p = cl.lit(j);
            if (qAssignments.isUndef(Literal.var(p))) {
              lit = p;
              break;
            }
          }
          assert lit != null : "clause reported to be unit is not actually unit, must be SAT";
          unitLits[i] = lit;
        }
      } else if (totalSat == DB.numClauses()) {
        result_type = QResult.Type.SAT;
        clauseIds = Collections.emptyList();
        unitLits = null;
        assert unitClauseIDs.isEmpty() : "all clauses are SAT yet there are units?!?! "
            + unitClauseIDs;
        assert conflictClauseIDs.isEmpty() : "all clauses are SAT yet there are conflicts?!?! "
            + conflictClauseIDs;
      } else {
        // unresolved: no conflicts, no units, perhaps not all clauses are SAT
        result_type = QResult.Type.UNRESOLVED;
        clauseIds = Collections.emptyList();
        unitLits = null;
        assert unitClauseIDs.isEmpty() : "unresolved yet there are units?!?! " + unitClauseIDs;
        assert conflictClauseIDs.isEmpty() : "unresolved yet there are conflicts?!?! "
            + conflictClauseIDs;
      }


      // Create result object.
      final QResult result = new QResult(result_type, clauseIds, unitLits);

      q.setQueryResult(result);
    }

    // clear host memories for next cycle
    for (final Mem m : memories()) {
      m.h_clear();
    }
  }

  /** Sub-classes that do not collect total stats can return false; default is true. */
  protected boolean shouldCheckTotalStats() {
    return true;
  }

  /** Sub-classes use this method to remember the results of consolidation in their mems. */
  protected abstract void consolidateToMems(int[] literals, int[] lit2clause, int[] offsets,
      int[] lengths);

}
