package ca.uwaterloo.miramichi.gpu;

import static org.jocl.CL.clGetDeviceInfo;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.jocl.CL;
import org.jocl.Pointer;
import org.jocl.Sizeof;
import org.jocl.cl_context;
import org.jocl.cl_device_id;
import org.jocl.cl_kernel;
import org.jocl.cl_platform_id;
import org.jocl.cl_program;
import org.jocl.example.JOCLDeviceQuery;

public final class OpenCLUtil {

  private OpenCLUtil() {
    throw new UnsupportedOperationException("a collection of public static utility methods");
  }

  public static cl_platform_id[] getPlatforms() {
    // The platform, device type and device number

    // Obtain the number of platforms
    int numPlatformsArray[] = OpenCLUtil.initOpenCLResult();
    CL.clGetPlatformIDs(0, null, numPlatformsArray);
    int numPlatforms = numPlatformsArray[0];

    // Obtain a platform ID
    cl_platform_id[] platforms = new cl_platform_id[numPlatforms];
    CL.clGetPlatformIDs(platforms.length, platforms, null);
    return platforms;
  }

  public static class GPUDeviceResult {
    public GPUDeviceResult(cl_device_id device, cl_platform_id platform) {
      this.device = device;
      this.platform = platform;
    }

    public cl_device_id device;
    public cl_platform_id platform;
  }

  public static GPUDeviceResult getGPUDevice(cl_platform_id[] platforms) {
    int platformIndex = 0;
    final int gpuIndex = 0;

    final long deviceType = CL.CL_DEVICE_TYPE_GPU;
    // Obtain the number of devices for the platform
    int numDevicesArray[] = OpenCLUtil.initOpenCLResult();

    for (; platformIndex < platforms.length; ++platformIndex) {
      int ret = CL.clGetDeviceIDs(platforms[platformIndex], deviceType, 0, null, numDevicesArray);
      if (ret != CL.CL_DEVICE_NOT_FOUND) {
        OpenCLUtil.checkErrorOpenCL(new int[] {ret}, "CL.clGetDeviceIDs");
        break;
      }
    }
    cl_platform_id platform = platforms[platformIndex];

    int numDevices = numDevicesArray[0];

    // Obtain a device ID
    final cl_device_id[] devices = new cl_device_id[numDevices];
    CL.clGetDeviceIDs(platform, deviceType, numDevices, devices, null);
    return new GPUDeviceResult(devices[gpuIndex], platform);
  }

  public static long getMaxWorkgroupSize(cl_device_id device) {
    return getSize(device, CL.CL_DEVICE_MAX_WORK_GROUP_SIZE);
  }

  public static long getWavefrontSize(cl_device_id device) {
    int size = (int) getSize(device, CL.CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE);
    if (0 == size) {
      String vendor = getString(device, CL.CL_DEVICE_VENDOR);
      if (vendor.startsWith("Advanced")) { // AMD
        size = 64;
      } else { // Presumably NVIDIA, if not NVIDIA 32 is the most reasonable default anyways.
        size = 32;
      }
    }
    return size;
  }

  /**
   * Returns the value of the device info parameter with the given name
   *
   * @param device The device
   * @param paramName The parameter name
   * @return The value
   *
   * @see JOCLDeviceQuery
   */
  private static long getSize(cl_device_id device, int paramName) {
    return getSizes(device, paramName, 1)[0];
  }

  /**
   * Returns the values of the device info parameter with the given name
   *
   * @param device The device
   * @param paramName The parameter name
   * @param numValues The number of values
   * @return The value
   *
   * @see JOCLDeviceQuery
   */
  private static long[] getSizes(cl_device_id device, int paramName, int numValues) {
    // The size of the returned data has to depend on
    // the size of a size_t, which is handled here
    ByteBuffer buffer =
        ByteBuffer.allocate(numValues * Sizeof.size_t).order(ByteOrder.nativeOrder());
    clGetDeviceInfo(device, paramName, Sizeof.size_t * numValues, Pointer.to(buffer), null);
    long values[] = new long[numValues];
    if (Sizeof.size_t == 4) {
      for (int i = 0; i < numValues; i++) {
        values[i] = buffer.getInt(i * Sizeof.size_t);
      }
    } else {
      for (int i = 0; i < numValues; i++) {
        values[i] = buffer.getLong(i * Sizeof.size_t);
      }
    }
    return values;
  }

  public static void releaseKernel(final cl_kernel k, final String name) {
    final int[] err = initOpenCLResult();
    err[0] = CL.clReleaseKernel(k);
    checkErrorOpenCL(err, "clReleaseKernel: " + name);
  }

  public static void releaseProgram(final cl_program p, final String name) {
    final int[] err = initOpenCLResult();
    err[0] = CL.clReleaseProgram(p);
    checkErrorOpenCL(err, "clReleaseProgram: " + name);
  }

  public static String getErrorString(int error) {
    switch (error) {
    // run-time and JIT compiler errors
      case 0:
        return "CL_SUCCESS";
      case -1:
        return "CL_DEVICE_NOT_FOUND";
      case -2:
        return "CL_DEVICE_NOT_AVAILABLE";
      case -3:
        return "CL_COMPILER_NOT_AVAILABLE";
      case -4:
        return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
      case -5:
        return "CL_OUT_OF_RESOURCES";
      case -6:
        return "CL_OUT_OF_HOST_MEMORY";
      case -7:
        return "CL_PROFILING_INFO_NOT_AVAILABLE";
      case -8:
        return "CL_MEM_COPY_OVERLAP";
      case -9:
        return "CL_IMAGE_FORMAT_MISMATCH";
      case -10:
        return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
      case -11:
        return "CL_BUILD_PROGRAM_FAILURE";
      case -12:
        return "CL_MAP_FAILURE";
      case -13:
        return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
      case -14:
        return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
      case -15:
        return "CL_COMPILE_PROGRAM_FAILURE";
      case -16:
        return "CL_LINKER_NOT_AVAILABLE";
      case -17:
        return "CL_LINK_PROGRAM_FAILURE";
      case -18:
        return "CL_DEVICE_PARTITION_FAILED";
      case -19:
        return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";

        // compile-time errors
      case -30:
        return "CL_INVALID_VALUE";
      case -31:
        return "CL_INVALID_DEVICE_TYPE";
      case -32:
        return "CL_INVALID_PLATFORM";
      case -33:
        return "CL_INVALID_DEVICE";
      case -34:
        return "CL_INVALID_CONTEXT";
      case -35:
        return "CL_INVALID_QUEUE_PROPERTIES";
      case -36:
        return "CL_INVALID_COMMAND_QUEUE";
      case -37:
        return "CL_INVALID_HOST_PTR";
      case -38:
        return "CL_INVALID_MEM_OBJECT";
      case -39:
        return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
      case -40:
        return "CL_INVALID_IMAGE_SIZE";
      case -41:
        return "CL_INVALID_SAMPLER";
      case -42:
        return "CL_INVALID_BINARY";
      case -43:
        return "CL_INVALID_BUILD_OPTIONS";
      case -44:
        return "CL_INVALID_PROGRAM";
      case -45:
        return "CL_INVALID_PROGRAM_EXECUTABLE";
      case -46:
        return "CL_INVALID_KERNEL_NAME";
      case -47:
        return "CL_INVALID_KERNEL_DEFINITION";
      case -48:
        return "CL_INVALID_KERNEL";
      case -49:
        return "CL_INVALID_ARG_INDEX";
      case -50:
        return "CL_INVALID_ARG_VALUE";
      case -51:
        return "CL_INVALID_ARG_SIZE";
      case -52:
        return "CL_INVALID_KERNEL_ARGS";
      case -53:
        return "CL_INVALID_WORK_DIMENSION";
      case -54:
        return "CL_INVALID_WORK_GROUP_SIZE";
      case -55:
        return "CL_INVALID_WORK_ITEM_SIZE";
      case -56:
        return "CL_INVALID_GLOBAL_OFFSET";
      case -57:
        return "CL_INVALID_EVENT_WAIT_LIST";
      case -58:
        return "CL_INVALID_EVENT";
      case -59:
        return "CL_INVALID_OPERATION";
      case -60:
        return "CL_INVALID_GL_OBJECT";
      case -61:
        return "CL_INVALID_BUFFER_SIZE";
      case -62:
        return "CL_INVALID_MIP_LEVEL";
      case -63:
        return "CL_INVALID_GLOBAL_WORK_SIZE";
      case -64:
        return "CL_INVALID_PROPERTY";
      case -65:
        return "CL_INVALID_IMAGE_DESCRIPTOR";
      case -66:
        return "CL_INVALID_COMPILER_OPTIONS";
      case -67:
        return "CL_INVALID_LINKER_OPTIONS";
      case -68:
        return "CL_INVALID_DEVICE_PARTITION_COUNT";

        // extension errors
      case -1000:
        return "CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR";
      case -1001:
        return "CL_PLATFORM_NOT_FOUND_KHR";
      case -1002:
        return "CL_INVALID_D3D10_DEVICE_KHR";
      case -1003:
        return "CL_INVALID_D3D10_RESOURCE_KHR";
      case -1004:
        return "CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR";
      case -1005:
        return "CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR";
      default:
        return "Unknown OpenCL error";
    }
  }

  public static void setKernelArg(final cl_kernel k, final String name, final int i,
      final int size, final Pointer p) {
    final int[] err = OpenCLUtil.initOpenCLResult();
    err[0] = CL.clSetKernelArg(k, i, size, p);
    OpenCLUtil.checkErrorOpenCL(err, "CL.clSetKernelArg, ", name);
  }

  /**
   * Obtain a single String containing the build logs of the given program for all devices that are
   * associated with the given program object.
   *
   * @param program The program object
   * @return The build logs, as a single string.
   */
  public static String obtainBuildLogs(cl_program program) {
    int numDevices[] = OpenCLUtil.initOpenCLResult();
    CL.clGetProgramInfo(program, CL.CL_PROGRAM_NUM_DEVICES, Sizeof.cl_uint, Pointer.to(numDevices),
        null);
    cl_device_id devices[] = new cl_device_id[numDevices[0]];
    CL.clGetProgramInfo(program, CL.CL_PROGRAM_DEVICES, numDevices[0] * Sizeof.cl_device_id,
        Pointer.to(devices), null);

    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < devices.length; i++) {
      sb.append("Build log for device " + i + ":\n");
      long logSize[] = new long[1];
      CL.clGetProgramBuildInfo(program, devices[i], CL.CL_PROGRAM_BUILD_LOG, 0, null, logSize);
      byte logData[] = new byte[(int) logSize[0]];
      CL.clGetProgramBuildInfo(program, devices[i], CL.CL_PROGRAM_BUILD_LOG, logSize[0],
          Pointer.to(logData), null);
      sb.append(new String(logData, 0, logData.length - 1));
      sb.append("\n");
    }
    return sb.toString();
  }

  public static cl_program initProgram(final String file, final cl_context c,
      final List<String> prefix) {
    final int err[] = OpenCLUtil.initOpenCLResult();

    // Create the program from the source code and the prefix
    String kernelSrc = "";
    // prepend the prefix (should just be pre-processor directives)
    for (final String p : prefix) {
      kernelSrc += p;
      kernelSrc += System.lineSeparator();
    }
    // now open the file
    try {
      kernelSrc += OpenCLUtil.readFile(file, Charset.defaultCharset());
    } catch (IOException e) {
      System.err.println("Unable to read file containing kernel source: " + file);
      System.exit(-1);
    }
    final cl_program p = CL.clCreateProgramWithSource(c, 1, new String[] {kernelSrc}, null, err);
    OpenCLUtil.checkErrorOpenCL(err, "CL.clCreateProgramWithSource");
    return p;
  }

  public static cl_kernel initKernel(final cl_program program, final String fun) {
    final int[] err = OpenCLUtil.initOpenCLResult();

    // Build the program
    String build_options = "";// "-D DEBUG_OPENCL";
    err[0] = CL.clBuildProgram(program, 0, null, build_options, null, null);
    if (err[0] != 0)
      System.out.println(obtainBuildLogs(program));
    OpenCLUtil.checkErrorOpenCL(err, "clBuildProgram");

    // Create the kernel
    final cl_kernel kernel = CL.clCreateKernel(program, fun, err);
    OpenCLUtil.checkErrorOpenCL(err, "CL.clCreateKernel");
    return kernel;
  }

  /**
   * Helper method that prints an error message if the error code is not equal to CL.CL_SUCCESS,
   * which indicates a successful OpenCL operation.
   *
   * @param err The error code.
   * @param msg The message to display.
   */
  public static void checkErrorOpenCL(final int[] err, final String msg, final String name) {
    if (err[0] != CL.CL_SUCCESS) {
      System.err.println("OpenCL error " + getErrorString(err[0]) + ": " + msg + " " + name);
      System.exit(-1);
    }
  }

  public static void checkErrorOpenCL(final int[] err, final String msg) {
    checkErrorOpenCL(err, msg, "");
  }

  /**
   * This methods returns a string containing the content of a text file whose location is specified
   * by path.
   *
   * @param path The location of the file.
   * @param encoding The character encoding of the file.
   * @return A String containing the contents of the file.
   * @throws IOException
   */
  public static String readFile(String path, Charset encoding) throws IOException {
    byte[] encoded = Files.readAllBytes(Paths.get(path));
    return new String(encoded, encoding);
  }

  public static Pointer pointerToInt(final int x) {
    return Pointer.to(new int[] {x});
  }

  /**
   * @return A blank array of size 1 used to hold an OpenCL result.
   */
  public static int[] initOpenCLResult() {
    // We allocate an array of ints to store OpenCL results due to a quirk of the JOCL interface,
    // where such arrays correspond with using pointers in the OpenCL C API.
    return new int[1];
  }

  static String getString(cl_device_id device, int paramName) {
    // Obtain the length of the string that will be queried
    long size[] = new long[1];
    CL.clGetDeviceInfo(device, paramName, 0, null, size);

    // Create a buffer of the appropriate size and fill it with the info
    byte buffer[] = new byte[(int) size[0]];
    CL.clGetDeviceInfo(device, paramName, buffer.length, Pointer.to(buffer), null);

    // Create a string from the buffer (excluding the trailing \0 byte)
    return new String(buffer, 0, buffer.length - 1);
  }

}
