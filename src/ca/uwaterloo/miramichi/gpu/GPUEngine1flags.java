package ca.uwaterloo.miramichi.gpu;

import java.util.logging.Logger;

import ca.uwaterloo.miramichi.ClauseDB;

public class GPUEngine1flags extends GPUEngine1 {

  public GPUEngine1flags(final ClauseDB DB, final int num_explorers) {
    super(DB, num_explorers);
  }

  @Override
  protected String kernelFile() {
    return "src/ca/uwaterloo/miramichi/gpu/original_flags.cl";
  }

  private static final Logger logger = Logger.getLogger(GPUEngine1flags.class.getSimpleName());

  /** For access from super-class. */
  @Override
  public Logger logger() {
    return logger;
  }

}
