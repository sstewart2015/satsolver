package ca.uwaterloo.miramichi.gpu;

import org.jocl.Pointer;
import org.jocl.Sizeof;
import org.jocl.cl_kernel;

public final class MemByte extends Mem {

  static cl_kernel zero_kernel = null;

  public byte[] h = null;

  public MemByte(final String name, final MemConfig config, final MemCapacity capacity) {
    super(name, config, capacity);
  }

  @Override
  public int deviceSizeUnit() {
    return Sizeof.cl_uchar;
  }

  @Override
  public void initHost() {
    // initialize mem on CPU
    final int h_capacity = capacity.hostCapacity();
    if (h_capacity <= 0) {
      // this will be set dynamically by the client
      h = null;
    } else {
      // this is a static buffer that we initialize here
      h = new byte[h_capacity];
    }
  }

  @Override
  public Pointer h_pointer() {
    return Pointer.to(h);
  };

  @Override
  public boolean h_isNull() {
    return null == h;
  }

  @Override
  public void h_clear() {
    if (null != h) {
      h = new byte[h.length];
    }
  }

  @Override
  public int h_length() {
    return (null == h) ? 0 : h.length;
  }

  @Override
  public void releaseHost() {
    h = null;
  }

  @Override
  public cl_kernel d_zero_kernel() {
    return zero_kernel;
  }
}
