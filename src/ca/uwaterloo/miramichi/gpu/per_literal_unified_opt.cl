// will be defined in Java code before kernel is loaded
//define WORKGROUPSIZE 256
//define MULTI_FACTOR 11

// Macros for literals
#define LIT_SIGN(p) ((p) & 1)
#define LIT_VAR(p) ((p) >> 1)

// Lifted Boolean domain
#define LBOOL_TRUE 0
#define LBOOL_FALSE 1
#define LBOOL_UNDEF 2

// Statistics
#define STAT_BLOCK_SIZE 5
#define STAT_OFFSET_SAT 0
#define STAT_OFFSET_UNIT 1
#define STAT_OFFSET_CONFL 2
#define STAT_OFFSET_UNRES 3
#define STAT_OFFSET_WASTE 4


// Inspect one literal for one explorer
__kernel void per_literal_unified(
    // inputs
    const int num_explorers,
    const int num_variables,
    const int num_literals,
    const int num_clauses,
    __global const char* const assignments,     // size == num_explorers * num_variables
    __global const int*  const literals,        // size == sum of length of all clauses
    __global const int*  const lit2clause,      // size == same as literals[]
    __global const int*  const clause_lengths,  // size == number of clauses
    __global const int*  const pageMap,         // size == num_pages
    // MULTI_FACTOR is set by the pre-processor, not an argument
    // outputs
    // initialized to zeroes
    __global int* const units,      // size == num_explorers * MULTI_FACTOR
    __global int* const conflicts,  // size == num_explorers * MULTI_FACTOR
    // initialized to zeroes
    __global int* const stats       // size == num_explorer * STAT_BLOCK_SIZE

    ) {

  // one cell for each clause being processed in this workgroup
  // the maximum number of clauses that could be processed is the size of the workgroup
  __local int num_lit_undefs_local[WORKGROUPSIZE];

  // one local cell corresponding to each global cell in cid
  // sometimes we index this by threadID (if we want to get every cell)
  // sometimes we index by cid_local_ndx (to store specific data)
  __local int units_local[MULTI_FACTOR];
  __local int conflicts_local[MULTI_FACTOR];

  // decode work item
  const int explorerID = get_global_id(0);
  const int threadID = get_local_id(1);
  const int pageID = pageMap[get_group_id(1)];
  const int literalID = pageID * WORKGROUPSIZE + threadID;
  //const int literalID = get_global_id(1);
  
  // literalID might be greater than num_literals due to workgroup sizing
  const int inWorkgroup = (literalID < num_literals);


  // these will be set below on the valid execution paths
  // the names need to be declared at this scope to avoid re-setting them
  // inside each workgroup-sizing conditional
  int clauseID = -1;
  int localClauseID = -1;
  int local_set = 0;
  int cid_local_ndx = -1;

  // initialize cid_local
  if (threadID < MULTI_FACTOR) {
    units_local[threadID] = 0; 
    conflicts_local[threadID] = 0; 
  }
  
  // initialize num_lit_undefs_local
    if (inWorkgroup) {
      // decode clauseID
    clauseID = lit2clause[literalID];
    localClauseID = clauseID % WORKGROUPSIZE;
      // initialize num_lit_undefs_local
      const int c_length = clause_lengths[clauseID];
      num_lit_undefs_local[localClauseID] = c_length;
   
    cid_local_ndx = clauseID % MULTI_FACTOR;

      // decode assignment
      const int p = literals[literalID];
      const int psign = LIT_SIGN(p);
      
      const int offset = NUM_VARIABLES_ALIGNED * explorerID;
      const int var_offset = offset + LIT_VAR(p);
      const int remainder = (var_offset & 0x3) << 1;
      const uchar assignment_loc = assignments[var_offset >> 2];

      const uchar assignment = (assignment_loc >> remainder) & 0x3;

      // decode the three cases: true, false, undefined
      const int a_true = (assignment==psign);
      const int a_undef = (assignment==LBOOL_UNDEF);
      const int a_false = !a_undef && !a_true;

    if (a_true) {
      // this clause is SAT for this explorer
      // prevent other threads from concluding unit or conflict
      atomic_xchg(&num_lit_undefs_local[localClauseID], -1);
    } else if (a_false) {
      atomic_dec(&num_lit_undefs_local[localClauseID]);
    }

    // record units locally
      const int undef = num_lit_undefs_local[localClauseID];
    if (undef == 1) {
      // unit
      // only set if nothing recorded in this slot
      atomic_cmpxchg(&units_local[cid_local_ndx], 0, -clauseID - 1);
      local_set = 1;
    } else if (undef == 0) {
      atomic_xchg(&conflicts_local[cid_local_ndx], clauseID + 1);
      local_set = 1;
    }
    } // end if
  
 
  // only upload to global where absolutely necessary
//  if (threadID < MULTI_FACTOR) {
  if (inWorkgroup && local_set) {
    const int u = units_local[cid_local_ndx];
    const int c = conflicts_local[cid_local_ndx];
    const int cid_ndx = (explorerID * MULTI_FACTOR) + cid_local_ndx;
    if (c > 0) {
      // conflict takes priority
      atomic_xchg(&conflicts[cid_ndx], c);
    } else if (u < 0) {
      // unit if nothing else recorded in this slot
      atomic_cmpxchg(&units[cid_ndx], 0, u);
    }
  }


} // end proc
