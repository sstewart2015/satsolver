package ca.uwaterloo.miramichi.gpu;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jocl.cl_device_id;
import org.jocl.cl_platform_id;

import ca.uwaterloo.miramichi.ClauseDB;
import ca.uwaterloo.miramichi.QueryEngine;

/**
 * A Factory for creating QueryEngines.
 */
public class GPUEngineFactory extends QueryEngine.Factory {

  private final Constructor<AbstractGPUEngine> c;
  private final int explorerCount;
  private final int engineNumber;

  private static final Logger logger = Logger.getLogger(GPUEngineFactory.class.getSimpleName());

  @SuppressWarnings("unchecked")
  public GPUEngineFactory(final String engineName, final int explorerCount) {
    this.explorerCount = explorerCount;
    final String substring = engineName.substring(3);
    final String n = getClass().getPackage().getName() + ".GPUEngine" + substring;
    logger.log(Level.INFO, n);
    engineNumber = substring.charAt(0);

    try {
      final Class<AbstractGPUEngine> cls = (Class<AbstractGPUEngine>) Class.forName(n);
      c = (Constructor<AbstractGPUEngine>) cls.getConstructors()[0];
    } catch (final ClassNotFoundException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  @Override
  public QueryEngine newEngine(final ClauseDB DB) {
    try {
      return c.newInstance(DB, explorerCount);
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  public int workgroupSize() {
    // get the GPU workgroup size
    final cl_platform_id[] platforms = OpenCLUtil.getPlatforms();
    final OpenCLUtil.GPUDeviceResult result = OpenCLUtil.getGPUDevice(platforms);
    final cl_device_id device = result.device;
    if (engineNumber >= 4) {
      return (int) OpenCLUtil.getWavefrontSize(device);
    } else {
      return (int) OpenCLUtil.getMaxWorkgroupSize(device);
    }
  }
}
