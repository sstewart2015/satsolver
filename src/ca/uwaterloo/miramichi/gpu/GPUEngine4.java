package ca.uwaterloo.miramichi.gpu;

import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Logger;

import org.jocl.Sizeof;
import org.jocl.cl_event;

import ca.uwaterloo.miramichi.ClauseDB;
import ca.uwaterloo.miramichi.Constants;
import ca.uwaterloo.miramichi.Util;


public class GPUEngine4 extends AbstractGPUEngine {

  /** All literals of all clauses. */
  private final MemInt literals = new MemInt("literals", MemConfig.IncrementalUploadOnly,
      new MemCapacity(-1, () -> MAX_NUM_LITERALS));

  /** Maps each literal index to the clause that literal belongs to. */
  private final MemInt lit2clause = new MemInt("lit2clause", MemConfig.IncrementalUploadOnly,
      new MemCapacity(-1, () -> MAX_NUM_LITERALS));

  /** Length of each clause. */
  private final MemInt clause_lengths = new MemInt("clause_lengths",
      MemConfig.IncrementalUploadOnly, new MemCapacity(-1, () -> MAX_NUM_CLAUSES));

  /**
   * Holds an encoded clause id (id of unit or conflicting clause) for each explorer. Also encoded
   * in this value is the result type (<0 = unit, >0 = conflict, 0 = done)
   */
  private final MemInt units = new MemInt("units", MemConfig.DownloadOnly, new MemCapacity(
      () -> numExplorers() * AbstractGPUEngine.MULTI_FACTOR));
  private final MemInt conflicts = new MemInt("conflicts", MemConfig.DownloadOnly, new MemCapacity(
      () -> numExplorers() * AbstractGPUEngine.MULTI_FACTOR));


  /** Array of all GPUMems instantiated above, so they can be processed in bulk. */
  private final Mem[] memories = {assignments, literals, lit2clause, clause_lengths, pageMap,
      units, conflicts, stats};

  public GPUEngine4(final ClauseDB DB, final int num_explorers) {
    super(DB, num_explorers);
    if (!DB.align) {
      throw new IllegalStateException("ClauseDB must be aligned to use GPU4");
    }

    computer.initialize();
    assert DB.align : "ClauseDB must be aligned for this kernel";
  }

  @Override
  protected void consolidateToMems(int[] literals, int[] lit2clause, int[] offsets, int[] lengths) {
    this.literals.h = literals;
    this.lit2clause.h = lit2clause;
    this.clause_lengths.h = lengths;
    assert num_literals_consolidated == this.literals.num_units_uploaded() + literals.length;
  }

  @Override
  protected boolean shouldCheckTotalStats() {
    return false;
  }

  @Override
  public void uploadPreprocessing() {
    if (Constants.areAssertionsEnabled()) {
      // check lit2clause has no hash collisions
      final SortedMap<Integer, Integer> modIndices = new TreeMap<>();
      final int start = lit2clause.num_units_uploaded();
      for (int i = 0; i < lit2clause.h.length; i++) {
        if ((start + i) % DB.workgroupSize == 0) {
          // new workgroup
          modIndices.clear();
        }
        final Integer clauseID = lit2clause.h[i];
        final int m = clauseID % DB.workgroupSize;
        final Integer old = modIndices.put(m, clauseID);
        if (old != null && !clauseID.equals(old)) {
          assert false : "hash collision! " + Util.i2s(old, clauseID, lit2clause.h.length, i);
        }
      }
    }
  }

  @Override
  public void uploadPostprocessing() {
    assert literals.num_units_uploaded() == DB.numLiterals() : "literals uploaded: "
        + literals.num_units_uploaded() + " DB.numLiterals() " + DB.numLiterals();
  }

  @Override
  public void downloadPreprocessing() {}

  @Override
  public void downloadPostprocessing() {
    // assert cid.h_length() == NUM_EXPLORERS * MULTI_FACTOR :
    // "cid.h_length != NUM_EXPLORERS * MULTI_FACTOR"
    // + cid.h_length() + " " + NUM_EXPLORERS + " " + MULTI_FACTOR;

    // merge units and conflicts into cid
    // TODO: move this somewhere else
    cid.h = new int[NUM_EXPLORERS * MULTI_FACTOR];
    for (int i = 0; i < cid.h_length(); i++) {
      final int u = units.h[i];
      final int c = conflicts.h[i];
      if (c > 0) {
        cid.h[i] = c;
      } else if (u < 0) {
        cid.h[i] = u;
      }
    }
  }

  @Override
  public Mem[] memories() {
    return memories;
  }


  @Override
  public Kernel[] kernels() {
    return kernels;
  }

  private final Kernel[] kernels = new Kernel[] {new MiramichiKernel(
      "src/ca/uwaterloo/miramichi/gpu/per_literal_unified_opt.cl", "per_literal_unified", DB) {

    @Override
    protected List<String> preprocessorDirectives() {
      final List<String> p = super.preprocessorDirectives();
      p.add("#define MULTI_FACTOR " + MULTI_FACTOR);
      return p;
    }

    @Override
    public cl_event prepareLaunchInner() {
      // sanity checks
      assert literals.num_units_uploaded() > 0;
      assert assignments.num_units_uploaded() > 0;
      assert lit2clause.num_units_uploaded() > 0;

      // inputs
      setArg("num_explorers", Sizeof.cl_int, OpenCLUtil.pointerToInt(NUM_EXPLORERS));
      setArg("num_variables", Sizeof.cl_int, OpenCLUtil.pointerToInt(DB.varCount()));
      setArg("num_literals", Sizeof.cl_int, OpenCLUtil.pointerToInt(literals.num_units_uploaded()));
      setArg("num_clauses", Sizeof.cl_int, OpenCLUtil.pointerToInt(num_clauses_consolidated));
      setArg("assignments", Sizeof.cl_mem, assignments.d_pointer());
      setArg("literals", Sizeof.cl_mem, literals.d_pointer());
      setArg("lit2clause", Sizeof.cl_mem, lit2clause.d_pointer());
      setArg("clause_lengths", Sizeof.cl_mem, clause_lengths.d_pointer());
      setArg("pageMap", Sizeof.cl_mem, pageMap.d_pointer());
      // outputs
      // setArg("d_cid", Sizeof.cl_mem, cid.d_pointer());
      setArg("d_units", Sizeof.cl_mem, units.d_pointer());
      setArg("d_conflicts", Sizeof.cl_mem, conflicts.d_pointer());
      setArg("d_stats", Sizeof.cl_mem, stats.d_pointer());

      // set kernel launch parameters
      final int work_x = NUM_EXPLORERS;
      // final int work_y = (int) fitToWorkgroup(literals.num_units_uploaded());
      final int work_y = pageMapAlias.length * (int) maxWorkgroupSize;
      final int work_items = work_x * work_y;
      total_work_items += work_items;

      work_dim = 2;
      global_work_offset = null;
      global_work_size = new long[] {work_x, work_y};
      local_work_size = new long[] {1, wavefrontSize};
      num_events_in_wait_list = 0;
      event_wait_list = null;
      event = AbstractGPUEngine.GPU_PROFILING ? new cl_event() : null;

      return event;
    }
  }};

  private static final Logger logger = Logger.getLogger(GPUEngine4.class.getSimpleName());

  /** For access from super-class. */
  @Override
  public Logger logger() {
    return logger;
  }

}
