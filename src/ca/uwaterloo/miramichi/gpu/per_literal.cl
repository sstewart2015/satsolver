// Macros for literals
#define LIT_SIGN(p) ((p) & 1)
#define LIT_VAR(p) ((p) >> 1)

// Lifted Boolean domain
#define LBOOL_TRUE 0
#define LBOOL_FALSE 1
#define LBOOL_UNDEF 2

// Statistics
#define STAT_BLOCK_SIZE 5
#define STAT_OFFSET_SAT 0
#define STAT_OFFSET_UNIT 1
#define STAT_OFFSET_CONFL 2
#define STAT_OFFSET_UNRES 3
#define STAT_OFFSET_WASTE 4


// Inspect one literal for one explorer
__kernel void per_literal(
    // inputs
    const int num_explorers,
    const int num_variables,
    const int num_literals,
    const int num_clauses,
    __global const char* const assignments, 	// size == num_explorers * num_variables
    __global const int* const literals, 		// size == sum of length of all clauses
    __global const int* const lit2clause, 		// size == same as literals[]
    // outputs
    __global char* const sat_flags, 			// size == num_explorers * num_clauses
    __global int* const num_lit_undefs,		// size == num_explorers * num_clauses
    __global char* const non_waste_flags	// size == num_explorers * num_clauses
    ) {

  // Obtain thread id
  const int tid = get_global_id(0);
  const int explorerID = tid / num_literals;
  const int literalID = tid - (explorerID * num_literals);

	// decode assignment
  const int offset = num_variables * explorerID;
  const int p = literals[literalID];
  const int psign = LIT_SIGN(p);
  const int var_offset = offset + LIT_VAR(p);
  const int remainder = (var_offset & 0x3) << 1;
  const uchar assignment_loc = assignments[var_offset >> 2];
  const uchar assignment = (assignment_loc >> remainder) & 0x3;

  // decode the three cases: true, false, undefined
  const int a_true = (assignment==psign);
  const int a_undef = (assignment==LBOOL_UNDEF);

  // some array indices we will need to record results of this literal inspection
  const int clauseID = lit2clause[literalID];
  const int explorerClauseID = (explorerID * num_clauses) + clauseID;

  if (a_true) {
    // we expect this clause to be true
    sat_flags[explorerClauseID] = 1;
  }

  if (a_undef) {
    // count the number of literals undefined
    atomic_inc(&num_lit_undefs[explorerClauseID]);
  } else {
    non_waste_flags[explorerClauseID] = 1;
  }
            
}
