package ca.uwaterloo.miramichi.gpu;

import java.util.List;
import java.util.logging.Logger;

import org.jocl.Sizeof;
import org.jocl.cl_event;

import ca.uwaterloo.miramichi.ClauseDB;


public class GPUEngine1 extends AbstractGPUEngine {

  /** All literals of all clauses. */
  private final MemInt literals = new MemInt("literals", MemConfig.IncrementalUploadOnly,
      new MemCapacity(-1, () -> MAX_NUM_LITERALS));

  /** Stores the starting index of each clause in the clause database. */
  private final MemInt offsets = new MemInt("offsets", MemConfig.IncrementalUploadOnly,
      new MemCapacity(-1, () -> MAX_NUM_CLAUSES));

  /** Array of all GPUMems instantiated above, so they can be processed in bulk. */
  private final Mem[] memories = {assignments, literals, offsets, cid, stats};

  public GPUEngine1(final ClauseDB DB, final int num_explorers) {
    super(DB, num_explorers);
    computer.initialize();
  }

  @Override
  protected void consolidateToMems(int[] literals, int[] lit2clause, int[] offsets, int[] lengths) {
    this.literals.h = literals;
    this.offsets.h = offsets;
    assert num_literals_consolidated == this.literals.num_units_uploaded() + literals.length;
    assert num_clauses_consolidated == this.offsets.num_units_uploaded() + offsets.length;
  }


  @Override
  public void uploadPreprocessing() {}

  @Override
  public void uploadPostprocessing() {
    assert literals.num_units_uploaded() == DB.numLiterals() : "literals uploaded: "
        + literals.num_units_uploaded() + " DB.numLiterals() " + DB.numLiterals();
    assert offsets.num_units_uploaded() == DB.numClauses() : "offsets.num_units_uploaded() == "
        + offsets.num_units_uploaded() + "  DB.numClauses() == " + DB.numClauses();
  }

  @Override
  public void downloadPreprocessing() {}

  @Override
  public void downloadPostprocessing() {
    assert cid.h_length() == NUM_EXPLORERS * MULTI_FACTOR : "cid.h_length != NUM_EXPLORERS * MULTI_FACTOR"
        + cid.h_length() + " " + NUM_EXPLORERS + " " + MULTI_FACTOR;
  }

  @Override
  public Mem[] memories() {
    return memories;
  }


  @Override
  public Kernel[] kernels() {
    return kernels;
  }

  protected String kernelFile() {
    return "src/ca/uwaterloo/miramichi/gpu/original.cl";
  }

  private final Kernel[] kernels = new Kernel[] {new MiramichiKernel(kernelFile(), "opencl_query",
      DB) {

    @Override
    protected List<String> preprocessorDirectives() {
      final List<String> p = super.preprocessorDirectives();
      p.add("#define MULTI_FACTOR " + MULTI_FACTOR);
      return p;
    }

    @Override
    public cl_event prepareLaunchInner() {
      // sanity checks
      assert literals.num_units_uploaded() > 0;
      assert offsets.num_units_uploaded() > 0;
      assert assignments.num_units_uploaded() > 0;

      // inputs
      setArg("num_variables", Sizeof.cl_int, OpenCLUtil.pointerToInt(DB.varCount()));
      setArg("assignments", Sizeof.cl_mem, assignments.d_pointer());
      setArg("num_explorers", Sizeof.cl_int, OpenCLUtil.pointerToInt(NUM_EXPLORERS));
      setArg("num_literals", Sizeof.cl_int, OpenCLUtil.pointerToInt(literals.num_units_uploaded()));
      setArg("literals", Sizeof.cl_mem, literals.d_pointer());
      setArg("num_clauses", Sizeof.cl_int, OpenCLUtil.pointerToInt(offsets.num_units_uploaded()));
      setArg("offsets", Sizeof.cl_mem, offsets.d_pointer());
      // outputs
      setArg("d_cid", Sizeof.cl_mem, cid.d_pointer());
      setArg("d_stats", Sizeof.cl_mem, stats.d_pointer());

      // set kernel launch parameters
      final int work_items = NUM_EXPLORERS * num_clauses_consolidated;
      total_work_items += work_items;

      work_dim = 1;
      global_work_offset = null;
      global_work_size = new long[] {work_items};
      local_work_size = null;
      num_events_in_wait_list = 0;
      event_wait_list = null;
      event = AbstractGPUEngine.GPU_PROFILING ? new cl_event() : null;

      return event;
    }
  }};

  private static final Logger logger = Logger.getLogger(GPUEngine1.class.getSimpleName());

  /** For access from super-class. */
  @Override
  public Logger logger() {
    return logger;
  }

}
