package ca.uwaterloo.miramichi;

/**
 * An implementation of a conflict-driven clause learning SAT-solver, largely based on the solver
 * architecture described in "Decision Procedures" (Korening and Strichman).
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import ca.uwaterloo.miramichi.seeding.SeedingStrategy;

public class MiniCDCL extends Explorer {

  /**
   * VSIDS Variable order. Note that all additions to this queue are made in Var, which aliases the
   * queue. MiniCDCL just removes items from the queue.
   */
  private final PriorityQueue<Var> order;

  /** Records of information about variables. */
  private final Var[] vars;

  /** Literals indexed by order of trail. */
  private final ArrayList<Integer> trail;

  /** Offset into trail for beginning of each decision level. */
  private final ArrayList<Integer> trailOffsets;

  private int rootLevel = 0;
  private Clause lastLearntClause = null;
  private int lastAssertingLiteral = -1;

  /** The next time we want to set this Var, we should use this assignment. */
  private final Map<Var, Boolean> nextBits = new LinkedHashMap<>();

  private final Random random = new Random(eid);

  /** Are we on our first step? */
  private boolean firstStep = true;

  /** Have we been reincarnated? */
  private final boolean reincarnated;

  /**
   * Are we operating under assumptions? If so, what appear to be a top-level conflict (i.e., global
   * UNSAT) might just in fact be a problem with the assumptions.
   */
  private boolean assumptions = false;

  private static final Logger logger = Logger.getLogger(MiniCDCL.class.getSimpleName());

  private MiniCDCL(final int eid, final boolean reincarnated, final ClauseDB db,
      final Assignment assigns) {
    super(eid, db, assigns);
    this.reincarnated = reincarnated;
    trail = new ArrayList<Integer>();
    trailOffsets = new ArrayList<Integer>();
    final int nVars = database.varCount();
    order = new PriorityQueue<Var>(nVars + 1); // a little breathing room
    // final int assignsStartOffset = eid * (int) Math.ceil(nVars / 4.0);
    // final int assignsEndOffset = assignsStartOffset + (int) Math.ceil(nVars / 4.0);
    // assert assignsStartOffset <= assigns.size();
    // assert assignsEndOffset <= assigns.size();
    this.vars = new Var[nVars];
    for (int i = 0; i < nVars; i++) {
      final Var v = new Var(i, order, assigns);
      vars[i] = v;
    }
  }

  @Override
  public boolean reincarnated() {
    return reincarnated;
  }

  /**
   * Chooses an unassigned variable and a truth value for it.
   *
   * @return false, if and only if there are no variables to assign
   */
  private boolean decide() {
    if (trail.size() == vars.length)
      return false; // no variables to assign

    // choose next unassigned variable, skipping over
    // currently assigned variables
    Var va = order.poll();
    while (!va.isUndef()) {
      va = order.poll();
      assert va != null : "No available decision.";
    }

    // we have a variable to assign
    trailOffsets.add(trail.size());

    // POLARITY
    // most variables are false most of the time, says Vijay
    final Boolean nextBit = nextBits.remove(va);
    final int p;
    if (nextBit == null) {
      // System.out.println("no nextBit:   " + va.var + "  " + nextBits);
      // we have freedom to choose polarity
      if (random.nextFloat() < 0.1) {
        // positive
        p = Literal.var2lit(va.var);
      } else {
        // negative
        p = Literal.neg(Literal.var2lit(va.var));
      }
    } else {
      // polarity is dictated
      // System.out.println("nextBit set: " + va.var + " " + nextBit);
      if (nextBit) {
        // positive
        p = Literal.var2lit(va.var);
      } else {
        // negative
        p = Literal.neg(Literal.var2lit(va.var));
      }
    }

    enqueue(p, null);
    logger.log(Level.FINEST, "Decide: {0} @ {1}", new Object[] {Literal.toString(p), va.level});
    return true;
  }

  /**
   * Responsible for computing the backtracking level, detecting global unsatisfiability, and adding
   * new constraints on the search in the form of new clauses.
   *
   * @return -1 if a top-level conflict; otherwise, the backtracking level.
   */
  private int analyze(final Clause confl) {
    assert confl != null;
    logger.log(Level.FINEST, "CONFLICT: " + confl + " @ " + decisionLevel());

    // detect top-level conflict
    if (decisionLevel() == this.rootLevel)
      return -1;

    // keeps track of variables already seen for tracing
    boolean[] seen = new boolean[vars.length];
    java.util.Arrays.fill(seen, false);

    // backtracking level
    int btLevel = -1;

    // number of literals at current decision level
    int count = 0;

    // literals of the learnt clause
    final Set<Integer> cl = new TreeSet<Integer>();
    int max_dl = -1;
    while (count == 0) {
      for (int j = 0; j < confl.size(); j++) {
        final int q = confl.lit(j);
        cl.add(q);
        int level = vars[Literal.var(q)].level;
        max_dl = Math.max(max_dl, level);
        if (vars[Literal.var(q)].level == decisionLevel())
          count++;
        seen[Literal.var(q)] = true;
      }
      if (count == 0) {
        backTrack(max_dl);
      }
    }

    logger.log(Level.FINEST, "=> " + literalsToString(cl, true));
    int i = 1; // tracks place in trail
    while (count > 1) {

      int p = -1; // last assigned literal
      int v = -1; // variable of p

      do {
        p = trail.get(trail.size() - i);
        i++;
        v = Literal.var(p);
      } while (!seen[v]);

      logger.log(Level.FINEST, "trace " + Literal.toString(p) + " sz=" + trail.size() + " i=" + i);

      // antecedent of p
      final Clause ante = vars[v].reason;
      if (ante == null) {
        assert vars[v].level == decisionLevel();
        break;
      }
      logger.log(Level.FINEST, "ante: " + ante);
      // should this assertion be true? it was added in late March and trips sometimes.
      // commented out for now.
      // assert ante.isSat(assigns);

      // trace p, resolving cl and ante with respect to v
      if (cl.contains(Literal.neg(p))) {
        cl.remove(Literal.neg(p));
        if (vars[v].level == decisionLevel())
          count--;
      }
      for (int j = 0; j < ante.size(); j++) {
        final int q = ante.lit(j);
        int qv = Literal.var(q);
        seen[qv] = true;
        if (qv == v)
          continue;
        if (!cl.contains(q)) {
          cl.add(q);
          if (vars[qv].level == decisionLevel())
            count++;
        }
      }

      logger.log(Level.FINEST, "cl = " + literalsToString(cl, true) + " count=" + count);
      assert countAtDecisionLevel(cl) == count : "countAtDecisionLevel(cl): "
          + countAtDecisionLevel(cl) + "  count: " + count;
    }

    // compute backtracking level (second highest in cl)
    // and determine asserting literal
    this.lastAssertingLiteral = -1;
    int max1 = Integer.MIN_VALUE;
    int max2 = Integer.MIN_VALUE;
    for (int q : cl) {
      final int level = vars[Literal.var(q)].level;
      if (level == decisionLevel())
        this.lastAssertingLiteral = q;
      if (level > max1) {
        max2 = max1;
        max1 = level;
      } else if (level > max2 && level < max1) {
        max2 = level;
      }
    }
    btLevel = Math.max(max2, rootLevel);
    assert btLevel > -1;

    // check asserting literal
    if (this.lastAssertingLiteral == -1) {
      // something has gone wrong
      // in the past this has been caused by a concurrency bug between workers and conductor
      // returning this value might cause the Worker to terminate the VM
      // return -2;
      System.out.println(confl.cid + " Total: " + database.numClauses());
    }
    assert this.lastAssertingLiteral > -1;
    assert vars[Literal.var(this.lastAssertingLiteral)].level == decisionLevel();
    logger.log(Level.FINEST, "Asserting literal is " + Literal.toString(lastAssertingLiteral));
    logger.log(Level.FINEST, "Learnt: " + literalsToString(cl, true) + " btLevel=" + btLevel);

    if (cl.size() <= 32) {
      // add learnt clause to db
      this.lastLearntClause = addClause(new ArrayList<Integer>(cl));
    }

    // decay activities after recording the learnt clause
    for (Var v : this.vars) {
      v.decayActivityLevel();
    }

    return btLevel;
  }

  private boolean enqueue(final int p, Clause from) {
    int v = Literal.var(p);
    if (!vars[v].isUndef()) {
      // this variable is already assigned
      // is the existing assignment the same as the requested one?
      if (!vars[v].isFalse())
        return false; // conflicting enqueued assignment
      return true; // existing consistent assignment - don't enqueue
    } else {
      // new fact, store it
      v = Literal.var(p);
      vars[v].assign(Literal.sign(p) ? Assignment.TRUE : Assignment.FALSE);
      vars[v].level = decisionLevel();
      vars[v].reason = from;
      trail.add(p);

      if (vars[v].reason != null)
        logger.log(Level.FINEST, "Enqueue: {0} @ {1} (from {2})", new Object[] {
            Literal.toString(p), vars[v].level, from.cid});
      return true;
    }
  }

  /**
   * Sets the current decision level to dl and erases assignments at decision levels larger than dl.
   */
  private void backTrack(final int dl) {
    while (decisionLevel() > dl) {
      // number of entries for current decision level to be undone
      final int lastIndex = trailOffsets.size() - 1;
      final int c = trail.size() - trailOffsets.get(lastIndex);
      for (int i = 0; i < c; ++i)
        popTrail();
      trailOffsets.remove(lastIndex);
    }

    logger.log(Level.FINE, "Backtrack to level " + dl);
  }

  /** Undoes one assignment at current decision level **/
  private void popTrail() {
    final int p = trail.remove(trail.size() - 1);
    vars[Literal.var(p)].reset();

    logger.log(Level.FINEST, "Popped " + Literal.toString(p) + " from trail");
  }

  private int decisionLevel() {
    return trailOffsets.size();
  }

  /** Adds a new clause containing the literals. **/
  private Clause addClause(final List<Integer> lits) {
    // bump activities for new clause's variables
    // ensure that the next time we stay consistent with the new learning
    for (final int p : lits) {
      final int v = Literal.var(p);
      final boolean sign = Literal.sign(p);
      final Var var = vars[v];
      var.increaseActivityLevel();
      nextBits.put(var, sign);
      // System.out.println("nextBits.put: " + var.var + " " + var.assignmentAsInt());
    }
    return database.learn(lits);
  }

  /** Can only be used safely before solving has started. **/
  @Override
  public boolean setAssumptions(final ArrayList<Integer> assumptions) {
    if (assumptions.isEmpty()) {
      // reset this solver
      while (!trail.isEmpty()) {
        popTrail();
      }
      return true;
    } else {
      // enqueue assumptions
      for (int i = 0; i < assumptions.size(); i++)
        if (!enqueue(assumptions.get(i), null))
          return false; // conflicting assignment
      this.assumptions = true;
      return true;
    }
  }

  @Override
  public Status step() {

    logger.log(Level.FINEST, "MiniStep: " + eid + " " + Thread.currentThread().getName());

    /** First time entering step() **/
    if (qresult == null) {
      assert firstStep;
      firstStep = false;
      decide();
      database.query(this);
      return Status.QUERY;
    }
    assert qresult != null;
    assert !firstStep;

    /** Process query result **/
    if (qresult.isConflict()) {
      // conflict found
      final Clause confl = database.lookup(qresult.clause_ids[0]);

      assert confl.isConfl(assigns) : "clause reported to be conflict for explorer " + eid
          + " is not. sat? " + confl.isSat(assigns) + " unit? " + confl.isUnit(assigns)
          + "\nqresult.clause_ids: " + Arrays.toString(qresult.clause_ids) + "\nclause: " + confl
          + "  " + confl.getSatLiterals(assigns) + "\nvars:   " + Arrays.toString(vars);
      final int btLevel = analyze(confl); // note: analyze enqueues asserting literal into propQ
      if (btLevel < 0) {
        if (btLevel == -2) {
          // something has gone wrong
          // in the past this has been caused by a concurrency bug between workers and conductor
          // returning this value might cause the Worker to terminate the VM
          return Status.DIE;
        } else {
          if (assumptions) {
            // this might not be a global UNSAT
            // it might just be a problem with our assumptions
            return Status.REINCARNATE;
          } else {
            // global conflict
            return Status.UNSAT;
          }
        }
      } else {
        backTrack(btLevel);
        assert lastAssertingLiteral > -1;
        enqueue(lastAssertingLiteral, lastLearntClause);
        database.query(this);
        return Status.QUERY;
      }
    } else if (qresult.isUnit()) {
      // unit information to propagate
      Clause unitClause = database.lookup(qresult.clause_ids[0]);
      if (Constants.areAssertionsEnabled() && !unitClause.isUnit(assigns)) {
        System.out.println("\n" + Arrays.toString(vars));
        System.out.println("EID: " + this.eid + " CID: " + qresult.clause_ids[0] + "\n"
            + unitClause);
      }
      assert unitClause.isUnit(assigns) : "clause reported as unit for explorer " + eid
          + " is not actually unit.  conflict? " + unitClause.isConfl(assigns) + "  sat? "
          + unitClause.isSat(assigns);
      int unitLiteral = qresult.unit_literals[0];
      enqueue(unitLiteral, unitClause);
      database.query(this);
      return Status.QUERY;

    } else if (qresult.isSAT()) {
      // confirm result with CPUEngine
      assert CPUEngine.confirmSAT(this) : "Engine returned SAT, but CPUEngine disagrees";
      return Status.SAT;

    } else if (qresult.isUnresolved()) {
      // fixed point, make a new decision
      if (!decide()) {
        // confirm result with CPUEngine
        assert CPUEngine.confirmSAT(this) : "Engine returned unresolved, but we have no more variables to decide on, and CPUEngine says not SAT";
        return Status.SAT;

      } else {
        // we made a decision, submit the new query
        database.query(this);
        return Status.QUERY;
      }
    } else {
      assert false : "unknown qresult: " + qresult;
      return Status.DIE;
    }
  }

  /** If levels is true, it will print the level of each variable. **/
  private String literalsToString(Collection<Integer> lits, boolean levels) {
    StringBuilder s = new StringBuilder();
    s.append("(");
    int i = 0;
    for (int p : lits) {
      s.append(Literal.toString(p));
      if (levels) {
        s.append("/");
        s.append(vars[Literal.var(p)].level);
      }
      if (i + 1 < lits.size())
        s.append(" ");
      i++;
    }
    s.append(")");
    return s.toString();
  }

  /** Returns the number of literals at the current decision level under the variable assignment **/
  private int countAtDecisionLevel(final Collection<Integer> lits) {
    int count = 0;
    for (final int p : lits)
      if (vars[Literal.var(p)].level == decisionLevel())
        count++;
    return count;
  }

  public Var[] getVars() {
    return vars;
  }

  public static class Factory extends Explorer.Factory {

    public Factory(final int crewSize, final SeedingStrategy ss) {
      super(crewSize, ss);
    }

    @Override
    public Explorer instantiateExplorer(final int eid, final boolean reincarnated,
        final ClauseDB DB, final Assignment assigns) {
      return new MiniCDCL(eid, reincarnated, DB, assigns);
    }

  }

}
