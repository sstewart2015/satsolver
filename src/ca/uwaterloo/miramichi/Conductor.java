package ca.uwaterloo.miramichi;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * The Conductor is responsible for train departures and delivering passengers. The passengers are
 * {@link Explorer explorers}, who perform their work during the trip. When the explorers reach
 * their destination, their work has been completed. During an on-going trip, a new train comes
 * along and other Explorers hop on-board; the conductor ensures that the next departure does not
 * occur until the previous train has completed its journey.
 */
public final class Conductor implements Runnable {

  /**
   * Owned (not shared) reference to the query engine.
   *
   * @see CPUEngine
   * @see GPUEngine
   * @see ComparisonEngine
   */
  private final QueryEngine engine;

  /**
   * Owned (not shared) queue of queries waiting to be processed. When the train leaves the station
   * a new queue/train is allocated and referred to by this field. This queue is accessed both the
   * by Conductor thread (to launch the train) and by the Worker threads (to enqueue new queries).
   */
  private volatile BlockingQueue<Explorer> nextTrain = new LinkedBlockingQueue<Explorer>();

  /**
   * When this flag is set to false the conductor terminates. Flag is shared with all workers.
   *
   * @see Worker#work
   */
  private final AtomicBoolean work;
  private final AtomicBoolean exit;

  /**
   * Queue of Explorers waiting to be picked up by a Worker. The Conductor adds Explorers to this
   * queue and the Workers take them off. Accessed by multiple threads (Conductor and all Workers).
   *
   * @see Worker#pending
   */
  private final BlockingQueue<Explorer> pending;

  /**
   * Reference to the global shared clause database (CPU copy).
   */
  private final ClauseDB DB;


  /** Logger. */
  private static final Logger logger = Logger.getLogger(Conductor.class.getName());

  public Conductor(final QueryEngine engine, BlockingQueue<Explorer> pendingQueue,
      AtomicBoolean work, AtomicBoolean exit, ClauseDB DB) {
    this.engine = engine;
    this.pending = pendingQueue;
    this.work = work;
    this.exit = exit;
    this.DB = DB;
  }


  /**
   * Launches the next train; asks engine to compute results; puts the explorers back on the pending
   * queue; consolidates the learnt clauses. Exits after one run: no longer loops.
   *
   * @see QueryEngine#compute(BlockingQueue)
   * @see ClauseDB#consolidate()
   * @see QueryEngine#consolidateDB(List)
   */
  @Override
  public void run() {
    try {
      if (this.work.get() == false) {
        exit.set(true);
        return;
      }
      if (nextTrain.isEmpty()) {
        // nothing to do
        return;
      } else {
        // we have some queries to process
        // first, consolidate learning
        final List<Clause> learntClauses = DB.consolidate();
        // load the new clauses
        engine.consolidateDB(learntClauses);
        // swap for a new train
        final ArrayList<Explorer> train = new ArrayList<>();
        nextTrain.drainTo(train);
        assert pendingAndTrainAreDisjoint(train);
        // compute the queries
        // must be a blocking call
        // engine is responsible for communicating results back to explorers
        logger.log(Level.FINEST, "Conductor size: " + train.size());
        engine.compute(train);

        // put these explorers back in the Worker pending queue
        assert pendingAndTrainAreDisjoint(train);
        for (final Explorer e : train) {
          assert !pending.contains(e);
          assert !e.queryResultIsNull();
          pending.put(e);
        }
        return; // don't sit in loop now that we are using barrier
      }
    } catch (Throwable e) {
      exit.set(true);
      e.printStackTrace();
    }
  }

  /**
   * Submit a query for processing. This is only intended to be called by ClauseDB. All other
   * clients should submit their queries via ClauseDB.
   *
   * @param explorer explorer that wants a query processed
   *
   * @see ClauseDB#query(Explorer)
   */
  void submitQuery(final Explorer explorer) {
    try {
      nextTrain.put(explorer); // blocking call
    } catch (final InterruptedException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  private boolean pendingAndTrainAreDisjoint(final List<Explorer> train) {
    for (final Explorer q : train) {
      assert !pending.contains(q);
    }
    return true;
  }
}
