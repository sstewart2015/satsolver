package ca.uwaterloo.miramichi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

/**
 * Clause data base. Supports growth. Does not support pruning. Thread-safe. Singleton.
 */
public final class ClauseDB {
  private Conductor conductor;

  /** Owned (not shared) counter for clause ID numbers. */
  private final AtomicInteger nextCID = new AtomicInteger(0);

  /** Sum of the number of literals in all clauses. Includes padding. */
  private int numLiterals = 0;

  /** Sum of the number of literals in all recently learnt clauses (includes padding). */
  private int learntLiterals = 0;

  /** Number of padding literals in recently learned clauses. */
  private int learntPadding = 0;

  /** Number of padding literals that have been added to clauses. */
  private int numPadding = 0;

  /** Whether this db should be packed to align with workgroup boundaries. */
  public final boolean align;

  /** Size of workgroup to align to. */
  public final int workgroupSize;

  private final SortedSet<Integer> variables;

  /** Maps frequency to set of variables that occur with that frequency in initial clauses. */
  public final SortedMap<Integer, SortedSet<Integer>> var_histogram = new TreeMap<>();

  /** Read-only array of immutable clauses. Accessed by all Worker threads. */
  public volatile Clause[] clauses = new Clause[0];

  private final List<BitSet> varsInPage = new ArrayList<>();

  /** Write-only list of recently learnt clauses. Thread-safe. Accessed by all Worker threads. */
  private BlockingQueue<Clause> learnt = new LinkedBlockingQueue<Clause>();

  private final Map<Clause, Clause> clause_map = new HashMap<Clause, Clause>();

  /** The last legitimate variable, before the dummy variables start. */
  public final int maxLegitVar;
  public final Integer dummyVar;
  public Integer dummyLit;
  private final List<Integer> dummyLitList;

  private int cnfDuplicateClauseCount = 0;

  private ClauseDB(final File file, final boolean align) {
    this(file, align, -1);
  }

  private ClauseDB(final File file, final boolean align, final int workgroupSize) {
    // read input file
    this.workgroupSize = workgroupSize;
    this.align = align;
    List<Clause> readClauses = readCNF(file, nextCID);
    final SortedSet<Integer> vars = computeVarHistogram(readClauses);
    this.maxLegitVar = vars.last();

    // set initial clause_map
    for (final Clause c : readClauses) {
      final Clause old = clause_map.put(c, c);
      if (old != null) {
        // the input CNF file has a duplicate clause
        cnfDuplicateClauseCount++;
      }
    }
    final List<Clause> uniqueClauses = new ArrayList<>(clause_map.keySet());

    // add dummy var, if necessary
    if (align) {
      this.dummyVar = maxLegitVar + 1;
      this.dummyLit = Literal.neg(Literal.var2lit(dummyVar));
      final ArrayList<Integer> a = new ArrayList<>(1);
      a.add(dummyLit);
      this.dummyLitList = Collections.unmodifiableList(a);
      vars.add(dummyVar);
    } else {
      this.dummyVar = null;
      this.dummyLit = null;
      this.dummyLitList = Collections.emptyList();
    }
    // save the set
    variables = Collections.unmodifiableSortedSet(vars);

    numLiterals = 0;
    numPadding = 0;
    if (align) {
      // packing might increase the number of clauses and literals due to padding
      final List<Clause> packedClauses = pack(uniqueClauses, workgroupSize, dummyLitList);
      // renumber clauses to their new position
      clauses = new Clause[packedClauses.size()];
      int page = 0;
      for (int i = 0; i < clauses.length; i++) {
        final Clause tmp = packedClauses.get(i);
        final Clause c = new Clause(i, page, tmp);
        clauses[i] = c;
        numLiterals += c.size();
        if (numLiterals % workgroupSize == 0) {
          // page boundary
          page++;
        }
        if (isDummy(c)) {
          clause_map.put(c, c);
          numPadding++;
        }
      }
      final int expectedPage = Math.floorDiv(numLiterals, workgroupSize);
      assert page == expectedPage : Util.i2s(page, expectedPage);

    } else {
      // no packing
      // but we still might need to do some renumbering due to duplicate clauses in the input
      this.clauses = uniqueClauses.toArray(new Clause[uniqueClauses.size()]);
      for (int i = 0; i < clauses.length; i++) {
        final Clause tmp = clauses[i];
        final Clause c = new Clause(i, 0, tmp);
        clauses[i] = c;
        numLiterals += c.size();
      }
    }
    nextCID.set(clauses.length);

    for (int i = 0; i < clauses.length; i++) {
      final Clause c = clauses[i];
      addToPage(c);
    }

    // initial stats
    stats.initialClauseCount = numClauses();
    stats.initialLiteralCount = numLiterals();
    stats.fileName = file.getAbsolutePath();

    assert repOk();
  }

  private void addToPage(final Clause c) {
    // add all the variables in this clause to the appropriate BitSet
    final BitSet bs = getPage(c);
    for (int j = 0; j < c.size(); j++) {
      final int lit = c.lit(j);
      final int var = Literal.var(lit);
      bs.set(var);
    }
  }

  /**
   * Get the BitSet for the page this clause is in. Assumes that growth will be just off the end, so
   * allocating one more BitSet will be enough.
   *
   * @param cid clause ID
   * @return BitSet representing all of the variables that are involved with the page that this
   *         clause is part of.
   */
  private BitSet getPage(final Clause clause) {
    final int pageID = clause.page;
    if (align && clause.cid > workgroupSize) {
      // this check only makes sense if we are aligned
      // in the non-aligned case every clause will be on page 0
      assert pageID > 0 : "pageID should be greater than zero for clauseID " + clause.cid;
    }
    if (pageID >= varsInPage.size()) {
      varsInPage.add(new BitSet(varCount()));
    }
    final BitSet bs = varsInPage.get(pageID);
    return bs;
  }

  public boolean pageNeedsRecomputing(final int pageID, final Assignment assigns) {
    final BitSet bs = varsInPage.get(pageID);
    return bs.intersects(assigns.getDirtyVars());
  }

  public int pageCount() {
    final int size = varsInPage.size();
    assert size > 0 : "must have at least one page!";
    if (Constants.areAssertionsEnabled() && align) {
      // this check only makes sense in the aligned case
      // in the non-aligned case all clauses are on page 1
      final int minSize = (int) Math.ceil((double) numLiterals() / (double) workgroupSize);
      assert size >= minSize : "must have at least " + minSize + " pages for " + numLiterals()
          + " literals";
    }
    return size;
  }

  public boolean isDummy(final Clause c) {
    return align && c != null && c.size() == 1 && c.lit(0) == dummyLit;
  }

  public synchronized boolean repOk() {
    final int ai = align ? 1 : 0;
    assert clause_map.size() == clauses.length - numPadding + ai + learnt.size() - learntPadding : Util
        .i2s(clause_map.size(), clauses.length, numPadding, learnt.size(), learntPadding);
    assert nextCID.get() == clauses.length + learnt.size() : Util.i2s(nextCID.get(),
        clauses.length, learnt.size());
    if (align) {
      assert checkAlignment();
      assert dummyVar == maxLegitVar + 1;
      assert dummyVar == variables.size() - 1;
    } else {
      assert maxLegitVar == variables.size() - 1;
    }
    return true;
  }

  private static List<Clause> pack(final List<Clause> readClauses, final int workgroupSize,
      final List<Integer> dummyLitList) {
    // clauses are copied from readClauses into buckets,
    // then into packedClauses, and finally into this.clauses

    // sort clauses by length
    Collections.sort(readClauses, new ClauseLengthComparator());
    // align clauses to workgroup boundaries
    // put clauses into buckets by length
    final SortedMap<Integer, List<Clause>> buckets = new TreeMap<>();
    List<Clause> bucket = null;
    int lastSize = -1;
    for (int i = 0; i < readClauses.size(); i++) {
      final Clause c = readClauses.get(i);
      final int size = c.size();
      // take advantage of the clauses being sorted by size to avoid always looking up in buckets
      if (bucket == null || size != lastSize) {
        // we don't have the right bucket
        bucket = buckets.get(size);
        if (bucket == null) {
          bucket = new ArrayList<>();
          buckets.put(size, bucket);
        }
      }
      // we now have the right bucket
      bucket.add(c);
      lastSize = size;
    }

    // now we have the clauses in buckets
    // let's pack them into workgroups
    // greedy approach: take either an exact match or largest available
    int literalsPacked = 0;
    int padding = 0;
    final List<Clause> packedClauses = new ArrayList<Clause>((int) (readClauses.size() * 1.2));
    while (!buckets.isEmpty()) {
      final int litsRemaining = workgroupSize - (literalsPacked % workgroupSize);
      // see if there is an exact match
      List<Clause> b = buckets.get(litsRemaining);
      int inc;
      if (b == null) {
        // there wasn't an exact match to fill out the remainder
        // take the largest
        inc = buckets.lastKey();
        if (inc > litsRemaining) {
          // largest is too big
          // try smallest
          inc = buckets.firstKey();
          if (inc > litsRemaining) {
            // smallest is also too big
            // insert dummies
            for (int i = 0; i < litsRemaining; i++) {
              packedClauses.add(new Clause(-1, dummyLitList));
              padding++;
            }
            literalsPacked += litsRemaining;
            continue;
          }
        }
        b = buckets.get(inc);
      } else {
        // exact match!
        inc = litsRemaining;
      }
      final Clause c = b.remove(b.size() - 1);
      packedClauses.add(c);
      literalsPacked += inc;
      if (b.isEmpty()) {
        buckets.remove(inc);
      }
    }

    // if we didn't add a dummy clause, add it now
    // otherwise we'll end up learning it later, which will cause problems
    if (padding == 0) {
      packedClauses.add(new Clause(-1, dummyLitList));
    }
    return packedClauses;
  }

  private Clause newDummyClause() {
    return new Clause(nextCID.getAndIncrement(), dummyLitList);
  }

  private boolean checkAlignment() {
    int currentWorkgroupSize = 0;
    int literals_observed = 0;
    int currentPage = -1; // this will be bumped to zero inside the modulus conditional
    final BitSet currentVars = new BitSet(varCount());
    for (int i = 0; i < clauses.length; i++) {
      final Clause c = clauses[i];
      if ((currentWorkgroupSize % workgroupSize) == 0) {
        // alignment!
        currentWorkgroupSize = 0;
        currentPage++;
        // check currentVars
        if (i > 0) {
          final Clause cPrev = clauses[i - 1];
          assert currentVars.equals(getPage(cPrev)) : "vars differ on page " + currentPage + ": \n"
              + currentVars + "\n" + getPage(cPrev);
        }
        currentVars.clear();
      }
      currentWorkgroupSize += c.size();
      literals_observed += c.size();
      // check the current workgroup isn't too big
      final boolean b = currentWorkgroupSize <= workgroupSize;
      if (!b) {
        System.out.println(c);
        assert b : "current workgroup is too large: " + currentWorkgroupSize + " should be <= "
            + workgroupSize;
        return false;
      }
      // check page
      assert c.page == currentPage : "clause on wrong page: "
          + Util.i2s(c.cid, c.page, currentPage);
      // check vars
      for (int j = 0; j < c.size(); j++) {
        final int var = Literal.var(c.lit(j));
        currentVars.set(var);
      }
    }
    assert literals_observed == numLiterals;
    return true;
  }

  private static class ClauseLengthComparator implements Comparator<Clause> {

    @Override
    public int compare(final Clause c1, final Clause c2) {
      return c1.size() - c2.size();
    }

  }

  public static ClauseDB FromCNFFile(File file, final boolean sort, final int workgroupSize) {
    return new ClauseDB(file, sort, workgroupSize);
  }

  public static ClauseDB FromCNFFilePath(String path, final boolean sort, final int workgroupSize) {
    return FromCNFFile(new File(path), sort, workgroupSize);
  }

  /** Returns number of clauses in the read-only array. Does not reflect recent learning. **/
  public int numClauses() {
    return clauses.length;
  }

  /** Lookup a clause object by its ID number. */
  public Clause lookup(final int cid) {
    final Clause c = clauses[cid];
    assert c.cid == cid;
    return c;
  }

  /** Number of variables in the database. Does not change after CNF input file is parsed. */
  public int varCount() {
    return variables.size();
  }

  /** Number of literals in the database. Might grow based on clause learning. */
  public int numLiterals() {
    return numLiterals;
  }

  /** Check that a proposed clause uses legal variables. */
  private boolean checkClause(final List<Integer> lits) {
    if (variables == null)
      return true; // we're still reading the CNF
    for (final Integer p : lits) {
      final int v = Literal.var(p);
      assert variables.contains(v) : "variables does not contain: " + v;
    }
    return true;
  }

  /**
   * Create a new clause and put it in the queue of recently learnt clauses. Called by all Worker
   * threads, hence synchronized.
   *
   * @param lits the literals to be put into the new Clause object
   * @return the newly created Clause
   */
  public synchronized Clause learn(final List<Integer> lits) {
    try {
      assert repOk();
      // check if we already learned this clause
      final Clause tempClause = new Clause(-1, lits);
      final Clause lookup = clause_map.get(tempClause);
      if (lookup != null) {
        // we already learned this clause
        // return the old one
        stats.relearnt++;
        return lookup;
      } else {
        // new clause
        if (isDummy(tempClause)) {
          // learning the dummy clause
          // this will crash repOk() later ...
          System.err.println("learning dummy clause!!! expect a crash!!!");
        }
      }
      assert checkClause(lits);
      // we now have a legitimate clause
      // do we need to pad?

      if (align) {
        final int w = ((numLiterals + learntLiterals) % workgroupSize);
        if ((w + lits.size()) > workgroupSize) {
          // this new clause is too big
          // add some dummy clauses
          final int padding = workgroupSize - w;
          if (padding < 0) {
            System.err.println("negative padding! " + w);
            assert padding >= 0 : "padding should not be negative: " + padding;
          }
          assert workgroupSize == w + padding;
          for (int i = 0; i < padding; i++) {
            learnt.put(new Clause(nextCID.getAndIncrement(), dummyLitList));
            learntLiterals++;
            learntPadding++;
          }
        }
      }

      // store the real clause
      final int cid = nextCID.getAndIncrement();
      final Clause c = new Clause(cid, lits);
      clause_map.put(c, c);
      learnt.put(c);
      learntLiterals += c.size();
      assert repOk();
      return c;

    } catch (final InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Consolidate the recently learnt clauses into the main database. Replaces the old clauses array
   * with a new array. Only called by the Conductor thread. learnt.drainTo() should block worker
   * threads from adding new clauses. Therefore, this method does not need to be synchronized.
   *
   * @see #clauses
   * @see #learnt
   * @see Conductor#run()
   */
  public synchronized List<Clause> consolidate() {
    assert repOk();
    final int s = learnt.size();
    final ArrayList<Clause> a = new ArrayList<Clause>(s);
    final int s2 = learnt.drainTo(a);
    assert s == s2 : "synchronized keyword is supposed to prevent concurrent learning ...";
    // make a new array of all clauses
    final int clauseCount = clauses.length;
    final int newSize = clauseCount + s2;
    final Clause[] newClauses = new Clause[newSize];
    // copy old clauses into newClauses array
    System.arraycopy(clauses, 0, newClauses, 0, clauseCount);
    // copy new clauses into newClauses array
    int newLiterals = 0;
    int newPadding = 0;
    for (int i = 0; i < a.size(); i++) {
      final int page = align ? Math.floorDiv(numLiterals + newLiterals, workgroupSize) : 1;
      final Clause raw = a.get(i);
      final Clause c = new Clause(raw.cid, page, raw);
      final int index = i + clauseCount;
      assert index == c.cid;
      // put the new clause into the array
      newClauses[index] = c;
      newLiterals += c.size();
      if (isDummy(c)) {
        newPadding++;
      }
      addToPage(c);
    }
    assert newLiterals == learntLiterals;
    assert newPadding == learntPadding : Util.i2s(newPadding, learntPadding);
    numLiterals += newLiterals;
    numPadding += newPadding;
    learntLiterals = 0;
    learntPadding = 0;
    // replace old clauses array with newClauses array
    this.clauses = newClauses;
    assert repOk();
    return a;
  }

  /**
   * Put this query into the next batch.
   */
  public void query(final Explorer e) {
    conductor.submitQuery(e);
  }

  /** Adapted from kahina.org DIMACS parser **/
  private static List<Clause> readCNF(final File cnf, final AtomicInteger nextCID) {
    final List<Clause> readClauses = new ArrayList<>();

    try {

      final Scanner in = new Scanner(new FileInputStream(cnf));

      // ignore comment header
      String problemLine = in.nextLine();
      while (problemLine.startsWith("c"))
        problemLine = in.nextLine();

      // process the problem line
      final String[] params = problemLine.split(" ");
      if (!params[0].equals("p")) {
        System.err.println("ERROR: Dimacs CNF file appears to miss the problem line!");
        in.close();
        throw new RuntimeException("bad CNF file");
      }
      if (!params[1].equals("cnf")) {
        System.err.println("ERROR: Parsing a non-CNF Dimacs file with the Dimacs CNF parser!");
        in.close();
        throw new RuntimeException("bad CNF file");
      }

      // variables used for parsing
      String currentLine;
      String[] tokens;
      List<Integer> currentClause = new LinkedList<Integer>();

      // parse clauses and skip comment lines
      while (in.hasNext()) {
        // DIMACS splits information with a 0; however, nearly everybody
        // implements it with a new line
        currentLine = in.nextLine();
        tokens = currentLine.split("\\s");

        if (tokens[0].equals("c")) {
          continue; // skip comments
        } else {
          for (int i = 0; i < tokens.length; i++) {
            if (tokens[i].isEmpty())
              continue;
            Integer literal = Integer.parseInt(tokens[i]);

            if (literal == 0) { // reached end of clause
              // record clause
              readClauses.add(new Clause(nextCID.getAndIncrement(), currentClause));
              // HERE
              currentClause = new LinkedList<Integer>();
              break;
            } else {
              int v = Math.abs(literal) - 1;
              int p = literal < 0 ? Literal.neg(Literal.var2lit(v)) : Literal.var2lit(v);
              currentClause.add(p);
            }
          }
        }
      }
      in.close();
    } catch (FileNotFoundException e) {
      System.err.println("ERROR: Dimacs CNF file not found: " + cnf);
      throw new RuntimeException("bad CNF file");
    }

    // success!
    return readClauses;
  }

  private SortedSet<Integer> computeVarHistogram(final List<Clause> readClauses) {
    // count variables
    // first get the set of variables
    final TreeSet<Integer> vars = new TreeSet<Integer>();
    for (final Clause c : readClauses) {
      for (int i = 0; i < c.size(); i++) {
        final int p = c.lit(i);
        final int v = Literal.var(p);
        vars.add(v);
      }
    }
    assert vars.first() >= 0 : "negative vars not allowed: " + vars.first();

    // check that vars starts at 0 and is contiguous
    if (!vars.first().equals(0) || vars.last() != (vars.size() - 1)) {
      // either we don't start at zero or we aren't contiguous
      // let's remap the variables
      // figure out which vars are missing
      final SortedSet<Integer> missing = new TreeSet<>();
      final int missingCount = vars.last() - (vars.size() - 1);
      int lastv = -1;
      for (final Integer v : vars) {
        while (v != lastv + 1) {
          // we're missing everything from lastv up to v-1
          lastv++;
          // remap to self for now
          missing.add(lastv);
        }
        lastv = v;
      }
      assert missing.size() == missingCount;
      // figure out what to remap to
      final SortedMap<Integer, Integer> remap = new TreeMap<>();
      final Iterator<Integer> it = vars.descendingIterator();
      for (final Integer v : missing) {
        remap.put(it.next(), v);
      }
      System.out.println("variable remap: " + remap);
      // do the remapping
      for (int i = 0; i < readClauses.size(); i++) {
        final Clause c = readClauses.get(i);
        final Clause d = c.varRemap(remap);
        readClauses.set(i, d);
      }
      // drop the remapped variables
      for (final Map.Entry<Integer, Integer> e : remap.entrySet()) {
        vars.remove(e.getKey());
        vars.add(e.getValue());
      }
    }

    // assert that the set is contiguous and starts at zero
    assert vars.first().equals(0) : "first variable is not zero: " + vars.first();
    final int maxLegitVar = vars.last();
    assert maxLegitVar == vars.size() - 1 : "variable set is not contiguous " + vars;

    // compute the initial frequency of each legitimate variable
    final int[] initialVarFrequency = new int[maxLegitVar + 1];
    for (final Clause c : readClauses) {
      for (int i = 0; i < c.size(); i++) {
        final int p = c.lit(i);
        final int v = Literal.var(p);
        if (v <= maxLegitVar) {
          initialVarFrequency[v]++;
        }
      }
    }
    // map frequency to set of vars with that frequency
    for (int v = 0; v < initialVarFrequency.length; v++) {
      final int freq = initialVarFrequency[v];
      SortedSet<Integer> set = var_histogram.get(freq);
      if (set == null) {
        set = new TreeSet<Integer>();
        var_histogram.put(freq, set);
      }
      set.add(v);
    }

    return vars;
  }

  /**
   * The ClauseDB and the Conductor link to each other, and the ClauseDB is constructed first, so
   * this field remains null until the Conductor is constructed by Main. The cyclic reference is
   * necessary for query submission.
   *
   * @param conductor the conductor
   * @see Main
   * @see #query(Explorer, int, int[])
   */
  public void setConductor(Conductor conductor) {
    this.conductor = conductor;
  }

  public void print() {
    for (Clause c : clauses) {
      System.out.println(c);
    }
  }


  private final Stats stats = new Stats();

  private static final class Stats {
    public String fileName;
    int initialClauseCount = 0;
    int initialLiteralCount = 0;
    int relearnt = 0;
  }

  private static final Logger logger = Logger.getLogger(ClauseDB.class.getSimpleName());

  public void logstats() {
    final LogHelper L = new LogHelper(logger, 26);
    L.info("CNF file", stats.fileName);
    L.info("duplicate clauses in CNF", cnfDuplicateClauseCount);
    L.info("initial clause count", stats.initialClauseCount);
    L.info("final clause count", numClauses());
    L.info("learnt clause count", (numClauses() - stats.initialClauseCount));
    L.info("re-learnt clause count", stats.relearnt);
    L.info("initial literal count", stats.initialLiteralCount);
    L.info("final literal count", numLiterals());
    L.info("learnt literal count", (numLiterals() - stats.initialLiteralCount));
    L.info("padding literal count", numPadding);
    L.info("gpu workgroup size", workgroupSize);
  }
}
