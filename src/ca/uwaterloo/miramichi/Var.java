package ca.uwaterloo.miramichi;

import java.util.PriorityQueue;

/**
 * Records information about each variable, such as the value it is assigned, it's activity, it's
 * level, etc. The state of the search. Used to sort the priority queue of variables according to
 * activity level for VSIDS.
 */
public final class Var implements Comparable<Var> {
  public final int var;
  double activity;
  Clause reason;
  int level;
  final PriorityQueue<Var> order;
  public final Assignment assigns;

  Var(final int var, final PriorityQueue<Var> order, final Assignment assigns) {
    this.var = var;
    // this.offset = startOffset + (var >>> 2);
    // this.offset_remainder = (var & 0x3) << 1;
    this.order = order;
    this.assigns = assigns;
    reset();
  }

  public void reset() {
    order.remove(this);
    activity = 0d;
    reason = null;
    level = -1;
    assign(Assignment.UNDEF);
    order.add(this);
  }

  public void increaseActivityLevel() {
    increaseActivityLevel(1);
  }

  public void increaseActivityLevel(final int factor) {
    order.remove(this);
    activity += (factor * Constants.ACTIVITY_LEVEL_INCREMENT);
    order.add(this);
  }

  void decayActivityLevel() {
    order.remove(this);
    activity *= Constants.ACTIVITY_LEVEL_DECAY_RATE;
    order.add(this);
  }

  void assign(final int b) {
    assigns.set(var, b);
  }

  int assignmentAsInt() {
    return assigns.get(var);
  }

  boolean isUndef() {
    return assigns.isUndef(var);
  }

  boolean isTrue() {
    return assigns.isTrue(var);
  }

  boolean isFalse() {
    return assigns.isFalse(var);
  }

  @Override
  public int compareTo(final Var v) {
    // TODO: confirm that this is giving the correct ordering (part of VSIDS)
    return (int) (activity - v.activity);
  }

  @Override
  public String toString() {
    return Assignment.toString(assignmentAsInt());
    // return assignmentAsBool().toString();
    // return (var + 1) + "=" + assignmentAsBool();
  }

}
