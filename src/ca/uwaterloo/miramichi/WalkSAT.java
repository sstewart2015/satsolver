package ca.uwaterloo.miramichi;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import ca.uwaterloo.miramichi.seeding.SeedingStrategy;

public class WalkSAT extends Explorer {

  private int n = 0;
  private final Random r = new Random();

  public WalkSAT(int eid, ClauseDB db, Assignment assigns) {
    super(eid, db, assigns);
  }

  @Override
  public boolean reincarnated() {
    return false;
  }

  @Override
  public Status step() {
    logger.log(Level.FINEST, "WalkSATStep: " + eid + " " + Thread.currentThread().getName());

    // do we have a result?
    if (qresult != null) {
      // did we win?
      if (qresult.isSAT()) {
        System.out.println("WalkSAT wins!");
        // confirm the guess
        if (Constants.areAssertionsEnabled()) {
          for (final Clause c : database.clauses) {
            assert c.isSat(assigns);
          }
        }
        return Status.SAT;

      } else if (qresult.isConflict()) {
        // examine the conflict and flip some bits
        // for now we select a random clause and flip a random bit
        // TODO: be smarter about which bits to flip (also an open research topic)
        final int[] clauseIDs = qresult.clause_ids;
        assert clauseIDs.length > 0;
        final int cndx = r.nextInt(clauseIDs.length);
        final int cid = clauseIDs[cndx];
        final Clause c = database.lookup(cid);
        assert c.isConfl(assigns);
        final int litndx = r.nextInt(c.size());
        final int lit = c.lit(litndx);
        final int var = Literal.var(lit);
        final boolean flipsign = Literal.sign(lit);
        final int flipvalue = flipsign ? Assignment.TRUE : Assignment.FALSE;
        assert flipvalue != assigns.get(var) : Assignment.toString(flipvalue);
        assigns.set(var, flipvalue);
        database.query(this);
        return Status.QUERY;
      } else {
        // this should never happen
        throw new IllegalStateException(
            "WalkSAT should either be done or find a conflict. What happened?");
      }
    } else {

      // initial assignment
      // random, with bias to false
      for (int i = 0; i < assigns.size(); i++) {
        final int value = r.nextFloat() > 0.9 ? Assignment.TRUE : Assignment.FALSE;
        assigns.set(i, value);
      }
      database.query(this);
      return Status.QUERY;

    }

  }

  @Override
  public boolean setAssumptions(ArrayList<Integer> nextAssumptions) {
    return true;
  }

  private static final Logger logger = Logger.getLogger(WalkSAT.class.getSimpleName());


  public static class Factory extends Explorer.Factory {

    public Factory(final int crewSize, final SeedingStrategy ss) {
      super(crewSize, ss);
    }

    @Override
    public Explorer instantiateExplorer(int eid, boolean reincarnated, ClauseDB DB,
        Assignment assigns) {
      return new WalkSAT(eid, DB, assigns);
    }

  }
}
