package ca.uwaterloo.miramichi.seeding;

import java.util.List;

import ca.uwaterloo.miramichi.Explorer;

/**
 * Does nothing to seed the Explorer.
 */
public class NopSeedingStrategy extends SeedingStrategy {

  @Override
  public boolean seed(Explorer explorer) {
    return true;
  }

  @Override
  public void reseed(Explorer ex, List<Integer> lits) {
    return;
  }

}
