package ca.uwaterloo.miramichi.seeding;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;

import ca.uwaterloo.miramichi.Assignment;
import ca.uwaterloo.miramichi.ClauseDB;
import ca.uwaterloo.miramichi.Explorer;
import ca.uwaterloo.miramichi.Literal;
import ca.uwaterloo.miramichi.Util;

public class RandomAssignmentSeedingStrategy extends SeedingStrategy {

  private final int numberOfVarsToSeed;

  private final SeedingStrategy vsidsStrategy = new RandomVariableOrderingSeedingStrategy();

  public RandomAssignmentSeedingStrategy() {
    // TODO: set number of variables to seed as a function of initialCrewSize
    this(8);
  }

  public RandomAssignmentSeedingStrategy(final int n) {
    numberOfVarsToSeed = n;
  }

  @Override
  public boolean seed(final Explorer explorer) {
    assert !explorer.reincarnated();
    final Assignment assigns = explorer.assigns;
    // start with the units
    final SortedSet<Integer> setLits = new TreeSet<>(UnitSeedingStrategy.unitLits(explorer));
    // keep track of which variables we are setting so we don't set the same one twice
    final SortedSet<Integer> setVars = new TreeSet<>();
    for (final Integer lit : setLits) {
      final boolean b = setVars.add(Literal.var(lit));
      assert b : "conflicting units! " + Util.i2s(lit, Literal.var(lit));
    }
    // pick some random variables
    final Random r = new Random();
    for (int i = numberOfVarsToSeed; i > 0; i--) {
      // randomly pick a variable
      final int v = r.nextInt(assigns.size());
      if (!setVars.contains(v)) {
        // randomly set the sign
        final int vu = Literal.var2lit(v);
        final int vs = r.nextBoolean() ? vu : Literal.neg(vu);
        // add it to the list
        setLits.add(vs);
      }
    }
    final ArrayList<Integer> setLitsList = new ArrayList<>(setLits);
    final boolean result = explorer.setAssumptions(setLitsList);
    if (!result) {
      // calling setAssumptions with an empty list will cause it to reset
      explorer.setAssumptions(new ArrayList<>());
      // for extra measure, reset all of the individual variables
      // now, set VSIDS to explore these conflicting assumptions
      reseed(explorer, setLitsList);
    } else {
      // assumptions appear to be ok
      // seed VSIDS with frequent variables
      final ClauseDB db = explorer.database;
      final Integer largestFreq = db.var_histogram.lastKey();
      final SortedSet<Integer> bigvars = db.var_histogram.get(largestFreq);
      reseed(explorer, new ArrayList<Integer>(bigvars));
    }
    return true;
  }

  @Override
  public void reseed(final Explorer ex, final List<Integer> lits) {
    vsidsStrategy.reseed(ex, lits);
  }
}
