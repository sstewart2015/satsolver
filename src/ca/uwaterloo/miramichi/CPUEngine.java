package ca.uwaterloo.miramichi;

import java.util.Collections;
import java.util.List;

public class CPUEngine extends QueryEngine {

  public CPUEngine(ClauseDB DB) {
    super(DB);
  }

  @Override
  protected void computeInner(final List<Explorer> explorers) {
    explorers.forEach(e -> processQuery(e));
  }

  private void processQuery(final Explorer e) {
    // statistics
    int totalSat = 0;
    int totalUnres = 0;
    int totalUnit = 0;
    int totalConfl = 0;
    int totalWaste = 0;

    // there are potentially many clauses of interest and many unit literals
    // but we just communicate one of each back to the explorer
    int conflictClauseID = -1;
    int unitClauseID = -1;
    int unitLit = -1;

    // make local pointers to these immutable variables for convenience
    final Assignment assigns = e.assigns;
    final Clause[] clauses = DB.clauses;

    // inspect the clauses
    for (final Clause cl : clauses) {
      final int res = cl.clauseState(assigns);
      switch (res) {
        case (Clause.CONFLICT): {
          assert cl.isConfl(assigns);
          totalConfl++;
          conflictClauseID = cl.cid;
          break;
        }
        case (Clause.UNIT): {
          assert cl.isUnit(assigns);
          totalUnit++;
          unitClauseID = cl.cid;
          for (int i = 0; i < cl.size(); i++) {
            final int p = cl.lit(i);
            if (assigns.isUndef(Literal.var(p))) {
              unitLit = p;
              break;
            }
          }
          break;
        }
        case (Clause.SAT): {
          assert cl.isSat(assigns);
          totalSat++;
          break;
        }
        case (Clause.WASTE): {
          totalWaste++;
          break;
        }
        default: {
          totalUnres++;
          break;
        }
      }
    }

    // paramaters to be set conditionally
    final QResult.Type explorer_result_type;
    final int[] clauseIds;// = new int[1];
    final int[] unitLits;// = new int[1];
    if (totalConfl > 0) {
      explorer_result_type = QResult.Type.CONFLICT;
      clauseIds = new int[] {conflictClauseID};
      unitLits = null;
    } else if (totalUnit > 0) {
      explorer_result_type = QResult.Type.UNIT;
      clauseIds = new int[] {unitClauseID};
      unitLits = new int[] {unitLit};
    } else if (totalSat == DB.numClauses()) {
      explorer_result_type = QResult.Type.SAT;
      clauseIds = null;
      unitLits = null;
    } else {
      explorer_result_type = QResult.Type.UNRESOLVED;
      clauseIds = null;
      unitLits = null;
    }
    final QResult result = new QResult(explorer_result_type, clauseIds, unitLits);
    e.setQueryResult(result); // send result to explorer
  }

  public static QueryEngine.Factory FACTORY = new Factory();

  private static class Factory extends QueryEngine.Factory {

    @Override
    public QueryEngine newEngine(final ClauseDB DB) {
      return new CPUEngine(DB);
    }

  }

  /**
   * CPUEngine works with the clause database directly, so does nothing for consolidation.
   */
  @Override
  public void consolidateDBInner(final List<Clause> learntClauses) {
    return;
  }

  @Override
  protected void releaseResourcesInner() {
    return;
  }

  public static boolean confirmSAT(final Explorer e) {
    // check the assignment with CPUEngine
    final CPUEngine cpuEngine = new CPUEngine(e.database);
    cpuEngine.compute(Collections.singletonList(e));
    final QResult qresult = e.qresult;
    assert !qresult.isConflict() : "CPUEngine found a conflict! "
        + e.database.lookup(qresult.clause_ids[0]);
    assert !qresult.isUnit() : "CPUEngine found a unit! "
        + e.database.lookup(qresult.clause_ids[0]);
    assert qresult.isSAT() : "CPUEngine did not report SAT!";
    return qresult.isSAT();
  }
}
