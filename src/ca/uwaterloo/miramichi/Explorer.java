package ca.uwaterloo.miramichi;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import ca.uwaterloo.miramichi.seeding.NopSeedingStrategy;
import ca.uwaterloo.miramichi.seeding.SeedingStrategy;

/**
 * Represents the current state of a search. Mutable. This class is not thread-safe. It assumes that
 * the program will only use it from one worker thread at a time (not necessarily the same worker
 * thread each activation, but nevertheless only one worker thread at once).
 */
public abstract class Explorer {

  /**
   * Unique ID generator for Explorers. Intended to be used by subclasses only; however, Java
   * forbids use of protected visibility on static fields in interfaces. Not intended to be accessed
   * by clients.
   */
  final static AtomicInteger nextID = new AtomicInteger(Constants.FIRST_EXPLORER_ID);

  public final int eid;

  public final Assignment assigns;

  public final ClauseDB database;

  /** Holds the result of the most recent query returned by the engine. */
  protected QResult qresult = null;

  public Explorer(final int eid, final ClauseDB db, final Assignment assigns) {
    this.eid = eid;
    this.database = db;
    this.assigns = assigns;
  }

  public final void setQueryResult(final QResult qr) {
    // logger.log(Level.INFO, "MiniSetQueryResult: " + eid + " " +
    // Thread.currentThread().getName());
    assert qr != null : "null QResults disallowed";
    qresult = qr;
  }

  public final boolean queryResultIsNull() {
    return null == qresult;
  }

  /** The status of an explorer indicates where it's at in the searching procedure. */
  public static enum Status {
    QUERY, SAT, UNSAT, DIE, REINCARNATE
  }

  /** Returns true if this explorer has been reincarnated. */
  public abstract boolean reincarnated();

  /** Singleton instance of EIDComparator, for sorting lists of explorers by eid. */
  public static final Comparator<? super Explorer> EID_COMPARATOR = new EIDComparator();

  /** Comparator used for sorting lists of explorers by EID. */
  final static class EIDComparator implements Comparator<Explorer> {

    @Override
    public int compare(final Explorer e1, final Explorer e2) {
      final int eid1 = e1.eid;
      final int eid2 = e2.eid;
      return eid1 - eid2;
    }

  }

  /**
   * Prompts the explorer to advance one step in the search procedure.
   *
   * @return The status of the explorer after carrying out the step.
   */
  public abstract Status step();

  /**
   * Set assumptions for this explorer: that is, a list of literals that are being asserted.
   *
   * @param nextAssumptions
   */
  public abstract boolean setAssumptions(ArrayList<Integer> nextAssumptions);


  /**
   * A factory for creating new Explorers. Each Explorer needs a reference to shared clause
   * database.
   *
   * @see ClauseDB
   */
  public abstract static class Factory {

    public final int initialCrewSize;
    private final SeedingStrategy seedingStrategy;

    public Factory() {
      this(Constants.INITIAL_CREW_SIZE, new NopSeedingStrategy());
    }

    public Factory(final int crewSize, final SeedingStrategy ss) {
      this.initialCrewSize = crewSize;
      this.seedingStrategy = ss;
    }

    /**
     * Instantiate and seed a new Explorer.
     *
     * @param DB
     * @return The new seeded Explorer.
     */
    public final Explorer newExplorer(final ClauseDB DB, final Assignment assigns) {
      final int eid = Explorer.nextID.getAndIncrement();
      final Explorer ex = instantiateExplorer(eid, false, DB, assigns);
      seedingStrategy.seed(ex);
      return ex;
    }

    public final Explorer reincarnate(final Explorer expl) {
      if (expl instanceof MiniCDCL) {
        return reincarnate((MiniCDCL) expl);
      } else {
        // TODO: how to reincarnate other kinds of explorers?
        // TODO: implement Visitor pattern for double-dispatch?
        System.err.println("Not reincarnating: " + expl);
        return expl;
      }
    }

    public final Explorer reincarnate(final MiniCDCL expl) {
      // reset our portion of the shared assignment array
      // remember which variables were set
      final List<Integer> lits = new ArrayList<>();
      for (final Var v : expl.getVars()) {
        if (!v.isUndef()) {
          lits.add(v.assignmentAsInt());
          v.reset();
        }
      }
      // construct the new one
      final int eid = expl.eid;
      final Explorer ex = instantiateExplorer(eid, true, expl.database, expl.assigns);
      seedingStrategy.reseed(ex, lits);
      return ex;
    }

    /**
     * Should only be called by newExplorer().
     *
     * @param DB
     * @return A newly instantiated (but not seeded) Explorer.
     */
    public abstract Explorer instantiateExplorer(final int eid, final boolean reincarnated,
        final ClauseDB DB, final Assignment assigns);

  };

}
