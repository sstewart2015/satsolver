package ca.uwaterloo.miramichi;

/**
 * Stores multiple logical values in a single cell of an int array. "Index" refers to the logical
 * index of the value. "Cell" refers to the physical index, or to the bits in a cell.
 *
 */
public class PackedArray {

  /**
   * The data in this packed array.
   */
  private int[] cells;

  /** Number of bits used for each logical value. Must be a power of 2. */
  public final int bitwidth;

  private final int bitwidthLog2;

  /** Number of logical values that can be stored in a cell. */
  public final int valuesPerCell;

  private final int valuesPerCellLog2;

  private final int indexMask;

  /** Mask to extract values. */
  public final int valueMask;

  /** Physical capacity, not logical size. */
  private final int capacity;

  private int highestSetIndex = -1;

  /**
   * Construct a PackedArray. The actual bitwidth will be a power of two that is greater than or
   * equal to the requested bitwidth. The actual logical capacity will be at least as large as the
   * requested capacity.
   *
   * @param requestedBitwidth
   * @param requestedCapacity
   */
  public PackedArray(final int requestedBitwidth, final int requestedCapacity) {
    super();

    if (requestedBitwidth <= 0) {
      throw new IllegalArgumentException("requestedBitWidth must be greater than 0");
    }
    if (requestedBitwidth > 32) {
      throw new IllegalArgumentException("requestedBitWidth must be less than or equal to 32");
    }
    if (requestedCapacity < 0) {
      throw new IllegalArgumentException("requestedCapacity must be greater than or equal to 0");
    }

    if ((32 % requestedBitwidth) == 0) {
      this.bitwidth = requestedBitwidth;
    } else {
      // requested bitwidth does not divide evenly into a cell
      // increase it until it does
      this.bitwidth = 1 << (log2(requestedBitwidth) + 1);
    }
    assert (32 % bitwidth) == 0;
    this.valuesPerCell = 32 / bitwidth;
    this.valuesPerCellLog2 = log2(valuesPerCell);
    this.indexMask = createMask(valuesPerCellLog2);
    this.bitwidthLog2 = log2(bitwidth);
    this.valueMask = createMask(bitwidth);

    this.cells = new int[cellCount(requestedCapacity, valuesPerCell)];
    this.capacity = cells.length * valuesPerCell;
  }

  public static int createMask(final int width) {
    int m = 0;
    for (int i = 0; i < width; i++) {
      m = (m << 1) + 1;
    }
    return m;
  }

  public static int log2(final int x) {
    // log2 is the same as finding the most significant bit
    // https://graphics.stanford.edu/~seander/bithacks.html#IntegerLogObvious
    int v = x;
    int r = 0;
    while ((v >>= 1) != 0) {
      r++;
    }
    return r;
  }

  /** The number of logical elements in this PackedArray. */
  public int size() {
    return highestSetIndex + 1;
  }

  /** The logical capacity of this PackedArray. */
  public int capacity() {
    return capacity;
  }


  /**
   * If the bitwidth is 32 this will say Integer.MAX_VALUE (which is only 31 bits).
   *
   * @return
   */
  public int maxValue() {
    return (bitwidth < 32) ? valueMask : Integer.MAX_VALUE;
  }

  public static int cellCount(final int capacity, final int valuesPerCell) {
    return (int) Math.ceil((float) capacity / (float) valuesPerCell);
  }

  protected int cell(final int index) {
    // these expressions are equivalent when bitwidth is a power of 2
    // return index / valuesPerCell;
    return index >> valuesPerCellLog2;
  }

  protected int subcell(final int index) {
    return (index & indexMask) << bitwidthLog2;
  }

  public void append(final int value) {
    set(highestSetIndex + 1, value);
  }

  public void set(final int index, final int value) {
    // the mask for 32 bits (0xFFFFFFFF) is -1, so in that case we can't check value <= mask
    assert value <= valueMask || bitwidth == 32;
    final int cell = cell(index);
    assert cell < cells.length;
    final int subcell = subcell(index);
    set(cell, subcell, value);
    if (index > highestSetIndex) {
      highestSetIndex = index;
    }
  }

  private final void set(final int cell, final int subcell, final int value) {
    assert value <= valueMask || bitwidth == 32;
    cells[cell] &= ~(valueMask << subcell);
    cells[cell] |= value << subcell;
  }

  public int get(int index) {
    return get(cell(index), subcell(index));
  }

  private final int get(final int cell, final int subcell) {
    return (cells[cell] >>> subcell) & valueMask;
  }

  /**
   * Returns a reference to the underlying data. Clients should not mutate the data. Reference is
   * made available to facilitate serialization.
   */
  public final int[] getCells() {
    return cells;
  }

  public boolean hasLeftovers() {
    return (highestSetIndex >= 0) && (((highestSetIndex + 1) % valuesPerCell) != 0);
  }

  public PackedArray getLeftovers() {
    final int leftoverSize = ((highestSetIndex + 1) % valuesPerCell);
    // System.out.println("getLeftovers: " + Util.i2s(highestSetIndex, valuesPerCell,
    // leftoverSize));
    final PackedArray leftovers = new PackedArray(bitwidth, leftoverSize);
    for (int j = 0, i = highestSetIndex - leftoverSize + 1; i <= highestSetIndex; i++, j++) {
      // System.out.println("getLeftover:  " + Util.i2s(j, i));
      leftovers.set(j, get(i));
    }
    return leftovers;
  }
}
