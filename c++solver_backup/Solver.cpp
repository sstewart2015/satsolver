/**
 * @file    Solver.cpp
 * @author  Steven.Stewart@uwaterloo.ca
 */

#include <algorithm>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include "Solver.h"

namespace Solver_Namespace {

Constr* Solver::propagate() {
  while (propQ.size() > 0) {
    lit p = propQ.back();  // p is now the enqueued fact to propagate
    propQ.pop();

    vector<Constr*> tmp = watches[LIT_INDEX(p)];  // 'tmp' will contain the watcher list for 'p'
    watches[LIT_INDEX(p)].clear();

    for (size_t i = 0; i < tmp.size(); ++i) {
      if (!tmp[i]->propagate(*this, p)) {
        // Constraint is conflicting; copy remaining watches to 'watches[p]'
        // and return constraint
        for (size_t j = i + 1; j < tmp.size(); ++j)
          watches[LIT_INDEX(p)].push_back(tmp[j]);
        propQ = std::queue<lit>();  // clear queue
        return tmp[i];
      }
    }

  }
  return nullptr;
}

bool Solver::enqueue(const lit p, Constr *from) {
  if (valueLit(p) != lbool_undef) {
    if (valueLit(p) == lbool_false)
      return false;  // Conflicting enqueued assignment
    return true;  // Existing consistent assignment -- don't enqueue
  } else {
    // New fact, store it
    var v = LIT_VAR(p);
    assigns[v] = LIT_SIGN(p) ? lbool_false : lbool_true;  // LBOOL_BOOL_TO_LBOOL(LIT_NEG(LIT_SIGN(p)));
    level[v] = decisionLevel();
    reason[v] = from;
    trail.push_back(p);
    propQ.push(p);
    return true;
  }

}

void Solver::analyze(Constr *confl, std::vector<lit> &out_learnt, int &out_btlevel) {
  assert(confl);
  assert(out_learnt.size() == 0);
  assert(decisionLevel() > root_level);
  assert(valueLit(((Clause* )confl)->lits[1]) == lbool_false);

  vector<bool> seen;
  for (int i = 0; i < nVars(); ++i)
    seen.push_back(false);
  int counter = 0;
  lit p = LIT_UNDEF;
  vector<lit> p_reason;

  out_learnt.push_back(LIT_UNDEF);  // leave room for the asserting literal
  out_btlevel = 0;

  do {
    p_reason.clear();
    assert(confl != NULL);
    confl->calcReason(*this, p, p_reason);

    // Trace reason for p
    for (size_t j = 0; j < p_reason.size(); ++j) {
      lit q = p_reason[j];
      var v = LIT_VAR(q);
      if (!seen[v]) {
        seen[v] = true;
        if (level[v] == decisionLevel())
          counter++;
        else if (level[v] > 0) {  // exclude variables from decision level 0
          out_learnt.push_back(LIT_NEG(q));
          out_btlevel = std::max(out_btlevel, level[v]);
        }
      }
    }

    // Select next literal to look at
    do {
      p = trail.back();
      confl = reason[LIT_VAR(p)];
      undoOne();
    } while (!seen[LIT_VAR(p)]);
    counter--;

  } while (counter > 0);
  out_learnt[0] = LIT_NEG(p);
}

void Solver::record(std::vector<lit> &clause) {
  Clause *c = nullptr;  // will be set to created clause, or NULL if 'clause[]' is unit
  Clause::Clause_new(*this, clause, true, &c);
  enqueue(clause[0], c);
  if (c != NULL)
    learnts.push_back(c);
}

void Solver::undoOne() {
  lit p = trail.back();
  var x = LIT_VAR(p);
  assigns[x] = lbool_undef;
  reason[x] = nullptr;
  level[x] = -1;
  order.undo(x);
  trail.pop_back();

  while (undos[x].size() > 0) {
    undos[x].back()->undo(*this, p);
    undos[x].pop_back();
  }
}

bool Solver::assume(const lit p) {
  assert(propQ.size() == 0);
  trail_lim.push_back((int) trail.size());
  return enqueue(p);
}

void Solver::cancel() {
  assert(propQ.size() == 0);
  int c = (int) trail.size() - trail_lim.back();
  for (; c != 0; --c)
    undoOne();
  trail_lim.pop_back();
}

void Solver::cancelUntil(const int level) {
  while (decisionLevel() > level)
    cancel();
}

void Solver::reduceDB() {
  fprintf(stderr, "This feature (reduceDB) is not currently supported.\n");
  return;
}

lbool Solver::search(int nof_conflicts, int nof_learnts) {
  assert(root_level == decisionLevel());

  int conflictC = 0;
  var_decay = 0.95;
  cla_decay = 0.999;
  model_.clear();

  while (true) {
    Constr *confl = propagate();
    assert(propQ.size() == 0);

    if (confl != nullptr) {
      // Conflict
      conflictC++;
      vector<lit> learnt_clause;
      int backtrack_level;
      if (decisionLevel() == root_level)
        return lbool_false;
      analyze(confl, learnt_clause, backtrack_level);
      assert(backtrack_level >= root_level);
      cancelUntil(std::max(backtrack_level, root_level));
      record(learnt_clause);
      decayActivities();
    } else {
      // No conflict

      // Simplify the set of problem clauses
//      if (decisionLevel() == 0) {
//        simplifyDB();  // our simplifier cannot return false here
//      }

      if (nAssigns() == nVars()) {
        // Model found
        model_.resize(nVars());
        for (int i = 0; i < nVars(); ++i)
          model_[i] = (valueVar(i) == lbool_true);
        cancelUntil(root_level);
        return lbool_true;
//      } else if (conflictC >= nof_conflicts) {
//        // Reached bound on number of conflicts
//        cancelUntil(root_level);  // force a restart
//        return lbool_undef;
      } else {
        // New variable decision
        lit p = LIT_VAR_TO_LIT(order.select());  // may have heuristic for polarity here
        assert(valueLit(p) == lbool_undef);
        int z = assume(p);  // cannot return false
        assert(z);
      }
    }
  }
  return lbool_true;
}

Solver::Solver()
    : cla_inc(1),
      cla_decay(0.999),
      var_inc(1),
      var_decay(0.95),
      order(assigns, activity),
      root_level(0) {
}

var Solver::newVar() {
  int index = nVars();
  watches.push_back(vector<Constr*>());
  watches.push_back(vector<Constr*>());
  undos.push_back(vector<Constr*>());
  reason.push_back(NULL);
  assigns.push_back(lbool_undef);
  level.push_back(-1);
  activity.push_back(0);
  order.newVar();
  return index;
}

bool Solver::addClause(std::vector<lit> &literals) {
  Clause *c;
  if (Clause::Clause_new(*this, literals, false, &c)) {
    if (c && c->lits.size() > 0)
      constrs.push_back(c);
    return true;
  }
  return false;
}

bool Solver::simplifyDB() {
  assert(decisionLevel() == root_level);
  if (propagate() != nullptr)
    return false;
  return true;
  {
    int j = 0;
    for (size_t i = 0; i < learnts.size(); ++i) {
      if (learnts[i]->simplify(*this))
        learnts[i]->remove(*this);
      else
        learnts[j++] = learnts[i];
    }
    learnts.resize(learnts.size() - (learnts.size() - j));
  }
  {
    int j = 0;
    for (size_t i = 0; i < constrs.size(); ++i) {
      if (constrs[i]->simplify(*this))
        constrs[i]->remove(*this);
      else
        constrs[j++] = constrs[i];
    }
    constrs.resize(constrs.size() - (constrs.size() - j));
  }
  return true;
}

bool Solver::solve(const std::vector<lit> *assumptions) {
  double nof_conflicts = 100;
  double nof_learnts = nConstraints() / 3;
  lbool status = lbool_undef;

  // push incremental assumptions
  if (assumptions != NULL) {
    for (size_t i = 0; i < assumptions->size(); ++i) {
      if (!assume((*assumptions)[i]) || propagate() != NULL) {
        cancelUntil(0);
        return false;
      }
    }
  }
  root_level = decisionLevel();

  // solve
  while (status == lbool_undef) {
    status = search((int) nof_conflicts, (int) nof_learnts);
    nof_conflicts *= 1.5;
    nof_learnts *= 1.1;
  }

  cancelUntil(0);
  return (status == lbool_true);
}

void Solver::readDIMACS(const std::string fname, bool printFlag, int *nvars, int *nclauses) {
  int n = 0, m = 0;
  std::ifstream fin;
  fin.open(fname.c_str(), std::ios_base::in);

  if (!fin.is_open()) {
    fprintf(stderr, "Unable to open file.\n");
    fin.close();
    exit(EXIT_FAILURE);
  }

  int lineNo = 0;
  int clauseCount = 0;
  std::string str;
  const char delimiters[3] = " \t";

  while (fin.good()) {
    lineNo++;
    getline(fin, str);
    char *line = new char[str.size() + 1];
    line[str.size()] = '\0';
    memcpy(line, str.c_str(), str.size());

    vector<lit> clause;
    char *tok = strtok(line, delimiters);
    while (tok != NULL) {
      if (*tok == 'c')
        break;
      else if (*tok == 'p') {
        // read "cnf"
        tok = strtok(NULL, delimiters);
        if (tok == NULL || strncmp(tok, "cnf", 4)) {
          fprintf(stderr, "Error reading file. Expect 'cnf' on line %d.\n", lineNo);
          exit(EXIT_FAILURE);
        }

        // number of variables
        tok = strtok(NULL, delimiters);
        if (tok == NULL || !std::isdigit(*tok)) {
          fprintf(stderr, "Error reading file: invalid input on line %d.\n", lineNo);
          exit(EXIT_FAILURE);
        }
        n = atoi(tok);

        // number of clauses
        tok = strtok(NULL, delimiters);
        if (tok == NULL || !std::isdigit(*tok)) {
          fprintf(stderr, "Error reading file: invalid input on line %d.\n", lineNo);
          exit(EXIT_FAILURE);
        }
        m = atoi(tok);

        if (nvars)
          *nvars = n;
        if (nclauses)
          *nclauses = m;

        // Introduce the variables to the solver
        for (int i = 0; i < n; ++i)
          newVar();
      } else {
        int tmp = atoi(tok);
        if (tmp == 0) {
          assert(clause.size() > 0);
          if (addClause(clause)) {
            clauseCount++;
            if (printFlag) {
              printf("%d: [%d] ", lineNo, clauseCount);
              for (size_t j = 0; j < clause.size(); ++j)
                printf("%s%d%s", LIT_PRINT(clause[j]), j + 1 < clause.size() ? " " : "\n");
            }
          }
          clause.clear();
        } else {
          int tmp = atoi(tok);
          var v = abs(tmp) - 1;
          lit p = tmp < 0 ? LIT_NEG(LIT_VAR_TO_LIT(v)) : LIT_VAR_TO_LIT(v);
          clause.push_back(p);
        }
      }
      tok = strtok(NULL, delimiters);
    }
  }
}

}

//void Solver::print(const int cid) {
//  if (cid >= nConstraints())
//    throw std::runtime_error("Invalid clause id (" + std::to_string(cid) + ").");
//  int sat = 0;
//  int unassigned = 0;
//  size_t x, y;
//
//  x = cdb_limit_[cid];  // beginning of clause
//  y = std::min(cdb_limit_[cid + 1] - 1, cdb_.size() - 1);  // end of clause
//  for (; x <= y; ++x) {
//    lbool z = valueLit(cdb_[x]);
//    if (z == lbool_true) {
//      sat++;
//      break;
//    } else if (z == lbool_undef)
//      unassigned++;
//  }
//  if (sat)
//    printf("sat   ");
//  else if (unassigned == 1)
//    printf("unit  ");
//  else if (unassigned == 0)
//    printf("confl ");
//
//  x = cdb_limit_[cid];  // beginning of clause
//  y = std::min(cdb_limit_[cid + 1] - 1, cdb_.size() - 1);  // end of clause
//  printf("(");
//  for (; x <= y; ++x) {
//    var v = LIT_VAR(cdb_[x]);
//    lbool z = assigns[v];
//    bool sign = LIT_SIGN(cdb_[x]);
//    if (z == lbool_true) {
//      sat++;
//      printf("%s%d=%s%s", sign ? "-" : "", LIT_VAR(cdb_[x]) + 1, sign ? "F" : "T",
//             x < y ? " " : "");
//    } else if (z == lbool_undef) {
//      unassigned++;
//      printf("%s%d%s", sign ? "-" : "", LIT_VAR(cdb_[x]) + 1, x < y ? " " : "");
//    } else
//      printf("%s%d=%s%s", sign ? "-" : "", LIT_VAR(cdb_[x]) + 1, sign ? "T" : "F",
//             x < y ? " " : "");
//  }
//  printf(") ");
//}
//
//void Solver::printAll() {
//  for (int i = 0; i < cdb_limit_.size(); ++i) {
//    printf("%d: ", i + 1);
//    print(i);
//    printf("\n");
//  }
//}
//
//void Solver::printTrail() {
//  printf("trail(%zu):  ", trail.size());
//  for (int i = 0; i < trail.size(); ++i)
//    printf("%s%d@%d%s", LIT_PRINT(trail[i]), level[LIT_VAR(trail[i])],
//           i + 1 < trail.size() ? " " : "");
//  printf("\n");
//}
//
//}
