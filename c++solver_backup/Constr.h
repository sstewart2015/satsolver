/**
 * @file    Constr.h
 * @author  Steven Stewart (steven.stewart@uwaterloo.ca)
 */

#ifndef Constr_h
#define Constr_h

#include <vector>
#include "types.h"

namespace Solver_Namespace {

class Solver;

class Constr {

 public:
  virtual ~Constr() {
  }
  virtual void remove(Solver &s) = 0;
  virtual bool propagate(Solver &s, const lit p) = 0;
  virtual bool simplify(Solver &s) {
    return false;
  }
  virtual void undo(Solver &s, const lit p) {
  }
  virtual void calcReason(Solver &s, const lit p, std::vector<lit> &out_reason) = 0;

};

}

#endif // Constr.h
