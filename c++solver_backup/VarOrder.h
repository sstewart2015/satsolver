/**
 * @file    VarOrder.h
 * @author  Steven Stewart (steven.stewart@uwaterloo.ca)
 */

#ifndef VarOrder_h
#define VarOrder_h

#include <queue>
#include "types.h"

using std::vector;
using std::pair;
using std::priority_queue;

namespace Solver_Namespace {

struct VarOrder_Comparator {
  bool operator()(pair<var, double> i, pair<var, double> j) {
    return i.second < j.second;
  }
};

class VarOrder {

 public:
  VarOrder(const std::vector<lbool> &ref_to_assigns, const std::vector<double> &ref_to_activity);

  virtual ~VarOrder() {
  }
  virtual void newVar();  // Called when a new variable is created.
  virtual void update(var v);  // Called when variable has increased in activity.
  virtual void updateAll();  // Called when all variables have been assigned new activities.
  virtual void undo(var v);  // Called when variable is unbound (may be selected again)
  virtual var select();  // Called to select a new, unassigned variable.
  virtual bool inHeap(var v);  // Returns true if v is in the heap (unselected)

 private:
  vector<var> heap_;  // ordering of variables (a heap implementation)
  vector<int> heap_pos_;  // keeps track of the index of each variable in the heap (order_)
  const std::vector<lbool> &assigns_;  // reference to solver's partial assignment
  const std::vector<double> &activities_;  // reference to solver's variable activity values
};

}  // Solver_Namespace

#endif // VarOrder_h
