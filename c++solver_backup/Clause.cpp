/**
 * @file    Clause.cpp
 * @author  Steven Stewart (steven.stewart@uwaterloo.ca)
 */
#include <cassert>
#include <cstdio>
#include <map>
#include <iostream>
#include <utility>
#include "Clause.h"
#include "Solver.h"

namespace Solver_Namespace {

bool Clause::Clause_new(Solver &s, std::vector<lit> &ps, const bool learnt, Clause **out_clause) {
  *out_clause = NULL;

  // Normalize clause
  if (!learnt) {
    // @todo Clause normalization
  }

  if (ps.size() == 0)
    return false;
  else if (ps.size() == 1)
    return s.enqueue(ps[0]);  // unit facts are enqueued

  else {
    // Allocate clause
    Clause *c = new Clause();
    for (size_t i = 0; i < ps.size(); ++i)
      c->lits.push_back(ps[i]);

    c->learnt = learnt;
    c->activity = 0.0f;  // only relevant for learnt clauses

    if (learnt) {
      // pick a second literal to watch

      // Note: By design of analyze(), all literals are LBOOL_FALSE,
      // except for lits[0]. We ensure that lits[1] is the literal
      // with the highest decision level.
      assert(s.valueLit(ps[0]) == lbool_undef);
      int max_dl = s.level[LIT_VAR(ps[1])];
      int max_i = 1;
      for (size_t i = 2; i < ps.size(); ++i) {
        int curLevel = s.level[LIT_VAR(ps[i])];
        //printf("%s%d : i=%d level=%d\n", LIT_PRINT(ps[i]), i, s.level[LIT_VAR(ps[i])]);
        if (curLevel > max_dl) {
          max_dl = curLevel;
          max_i = i;
        }
      }
      //printf("max_dl=%d max_i=%d\n", max_dl, max_i);
      //std::swap(c->lits[1], c->lits[max_i]);
      if (max_i != 1) {
        c->lits[1] = ps[max_i];
        c->lits[max_i] = ps[1];
      }
//      for (int i = 0; i < ps.size(); ++i) {
//        if (c->lits[i] != ps[i]) {
//          for (int j = 0; j < ps.size(); ++j)
//            printf("%s%d %s%d @ %d\n", LIT_PRINT(c->lits[j]), LIT_PRINT(ps[j]),
//                   s.level[LIT_VAR(ps[j])]);
//          assert(true);
//          break;
//        }
//      }

      // Bumping
      s.claBumpActivity(c);  // newly learnt clauses should be considered active
      for (size_t i = 0; i < ps.size(); ++i) {
        s.varBumpActivity(LIT_VAR(ps[i]));  // variables in conflict clauses are bumped
      }
    }
    ps.clear();

    // Add clause to watcher lists
    s.watches[LIT_INDEX(LIT_NEG(c->lits[0]))].push_back(c);
    s.watches[LIT_INDEX(LIT_NEG(c->lits[1]))].push_back(c);
    *out_clause = c;

    return true;
  }
}

/// Learnt clauses only
bool Clause::locked(Solver &s) {
  return s.reason[LIT_VAR(lits[0])] == this;
}

void Clause::remove(Solver &s) {
  std::vector<Constr*> *w1 = &s.watches[LIT_INDEX(LIT_NEG(lits[0]))];
  std::vector<Constr*> *w2 = &s.watches[LIT_INDEX(LIT_NEG(lits[1]))];

  for (std::vector<Constr*>::iterator it = w1->begin(); it != w1->end(); ++it) {
    if (this == (Clause*) (*it)) {
      w1->erase(it);
      break;
    }
  }
  //s.printWatchLists();

  for (std::vector<Constr*>::iterator it = w2->begin(); it != w2->end(); ++it) {
    if (this == (Clause*) (*it)) {
      w2->erase(it);
      break;
    }
  }
  //s.printWatchLists();

  delete this;

}

bool Clause::propagate(Solver &s, const lit p) {
  // Make sure the false literal is lits[1]
  if (lits[0] == LIT_NEG(p)) {
    lits[0] = lits[1];
    lits[1] = LIT_NEG(p);
  }

  // If 0th watch is true, then clause is already satisfied
  if (s.valueLit(lits[0]) == lbool_true) {
    s.watches[LIT_INDEX(p)].push_back(this);  // re-insert clause back into watcher list
    return true;
  }

  // Look for a new literal to watch
  for (size_t i = 2; i < lits.size(); ++i) {
    if (s.valueLit(lits[i]) != lbool_false) {
      lits[1] = lits[i];
      lits[i] = LIT_NEG(p);
      s.watches[LIT_INDEX(LIT_NEG(lits[1]))].push_back(this);  // insert clause into watcher list
      return true;
    }
  }

  // Clause is unit under assignment
  s.watches[LIT_INDEX(p)].push_back(this);
  return s.enqueue(lits[0], this);  // enqueue for propagation
}

bool Clause::simplify(Solver &s) {
  assert(s.propQ.size() == 0);  // only called at top-level with empty propapgation queue
  assert(s.decisionLevel() == s.root_level);
  int j = 0;
  for (size_t i = 0; i < lits.size(); ++i) {
    if (s.valueLit(lits[i]) == lbool_true)
      return true;
    else if (s.valueLit(lits[i]) == lbool_undef)
      lits[j++] = lits[i];  // false literals are not copied (only occur for i >= 2)
  }
  lits.resize(lits.size() - (lits.size() - j));
  return false;
}

void Clause::calcReason(Solver &s, const lit p, std::vector<lit> &out_reason) {
  assert(p == LIT_UNDEF || p == lits[0]);
  for (size_t i = ((p == LIT_UNDEF) ? 0 : 1); i < lits.size(); ++i) {
    assert(s.valueLit(lits[i]) == lbool_false);
    out_reason.push_back(LIT_NEG(lits[i]));
  }
  if (learnt)
    s.claBumpActivity(this);
}

void Clause::print() {
  std::cout << "(";
  for (size_t i = 0; i < lits.size(); ++i)
    printf("%s%d%s", LIT_PRINT(lits[i]), i + 1 < lits.size() ? " " : "");
  std::cout << ")";
}

}
