/**
 * @file    Solver.h
 * @author  Steven.Stewart@uwaterloo.ca
 *
 * This file contains type definitions, classes and methods, etc., related
 * to the SAT-solver within the namespace "Solver_Namespace."
 */

#ifndef Solver_h
#define Solver_h

#include <cassert>
#include <vector>
#include <queue>
#include "Constr.h"
#include "Clause.h"
#include "types.h"
#include "VarOrder.h"

namespace Solver_Namespace {

// forward declarations
class VarOrder;

/**
 * @class Solver
 *
 * This class captures the state of the Solver, and it encapsulates the
 * the core functionality, including decisions, Boolean constraint
 * propagation, conflict analysis, and backtracking.
 */
class Solver {
  friend Clause;

 private:
  // Internal state of the Solver

  // Constraint database
  std::vector<Constr*> constrs;
  std::vector<Clause*> learnts;
  double cla_inc;
  double cla_decay;

  // Variable order
  std::vector<double> activity;
  double var_inc;
  double var_decay;
  VarOrder order;

  // Propagation
  std::vector<std::vector<Constr*>> watches;
  std::vector<std::vector<Constr*>> undos;
  std::queue<lit> propQ;

  // Assignment
  std::vector<lbool> assigns;
  std::vector<lit> trail;
  std::vector<int> trail_lim;
  std::vector<Constr*> reason;
  std::vector<int> level;
  int root_level;

  // Other
  std::vector<bool> model_;

 private:
  // Core solver methods (Decisions, BCP, Conflict Analysis, BackTrack)

  /**
   *
   *
   */
  Constr* propagate();

  /**
   *
   *
   */
  bool enqueue(const lit p, Constr *from = nullptr);

  /**
   *
   *
   */
  void analyze(Constr *confl, std::vector<lit> &out_learnt, int &out_btlevel);

  /**
   *
   *
   */
  void record(std::vector<lit> &clause);

  /**
   *
   *
   */
  void undoOne();

  /**
   *
   *
   */
  bool assume(const lit p);

  /**
   *
   *
   */
  void cancel();

  /**
   *
   *
   */
  void cancelUntil(const int level);

  /**
   *
   *
   */
  void reduceDB();

  /**
   *
   *
   */
  lbool search(int nof_conflicts, int nof_learnts);

 private:
  // Clause and variable activity-related methods

  INLINE void varBumpActivity(var x) {
    assert(x >= 0 && x < nVars());
    if ((activity[x] += this->var_inc) > 1e100)
      varRescaleActivity();
    if (order.inHeap(x))
      order.update(x);
  }

  INLINE void varDecayActivity() {
    var_inc *= var_decay;
  }

  INLINE void varRescaleActivity() {
    for (int i = 0; i < nVars(); ++i)
      activity[i] *= 1e-100;
    this->var_inc *= 1e-100;
  }

  INLINE void claBumpActivity(Clause *c) {
    if ((c->activity += this->cla_inc) > 1e100)
      claRescaleActivity();
  }

  INLINE void claDecayActivity() {
    cla_inc *= cla_decay;
  }

  INLINE void claRescaleActivity() {
    for (size_t i = 0; i < learnts.size(); ++i)
      learnts[i]->activity *= 1e-100;
    this->cla_inc *= 1e-100;
  }

  INLINE void decayActivities() {
    varDecayActivity();
    claDecayActivity();
  }

 public:
  // API (public interface)

  /// Constructor
  Solver();

  /**
   * Introduces a new variable
   * @return  Returns the new variable.
   */
  var newVar();

  /**
   * Adds a new clause to the database.
   *
   * @param[in]   literals    The literals of the new clause.
   * @return Returns true upon success.
   */
  bool addClause(std::vector<lit> &literals);

  /**
   * This method can be called prior to a call to solve() to
   * simplify the constraint database. It will propagate any
   * unit information and then removes the satisfied constraints.
   *
   * @return
   */
  bool simplifyDB();

  /**
   * This method carries out the solving procedure. An optional list
   * of assumptions may be passed, which are assumed by the solver
   * to be true.
   *
   * @param[in]   assumptions     Assumed to be true by the solver.
   * @return
   */
  bool solve(const std::vector<lit> *assumptions = nullptr);

  /**
   * Reads a text file in the DIMACS CNF format. The clauses parsed
   * from the file are added to the constraint database.
   *
   * @param[in]   fname       The name of the input file.
   * @param[in]   printFlag   If true, the parsed clauses are printed.
   */
  void readDIMACS(const std::string fname, bool printFlag, int *nvars, int *nclauses);

 public:
  // Small helper methods

  /// Returns a model (empty, if no model found)
  INLINE std::vector<bool> model() const {
    return model_;
  }

  /// Number of variables
  INLINE int nVars() {
    return (int) assigns.size();
  }

  /// Number of assignments (trail length)
  INLINE int nAssigns() {
    return (int) trail.size();
  }

  /// Number of constraints
  INLINE int nConstraints() {
    return (int) constrs.size();
  }

  /// Number of learnt clauses
  INLINE int nLearnts() {
    return (int) learnts.size();
  }

  /// Current value of variable x
  INLINE lbool valueVar(var x) {
    return assigns[x];
  }

  /// Current value of literal p
  INLINE lbool valueLit(lit p) {
    return LIT_SIGN(p) ? LBOOL_NEG(assigns[LIT_VAR(p)]) : assigns[LIT_VAR(p)];
  }

  /// Current decision level
  INLINE int decisionLevel() {
    return (int) trail_lim.size();
  }

};

}  // Solver_Namespace

#endif // Solver_h
