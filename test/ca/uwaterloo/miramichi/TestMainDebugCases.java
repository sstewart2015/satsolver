package ca.uwaterloo.miramichi;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.function.Supplier;
import java.util.logging.Logger;

import org.junit.Test;

public class TestMainDebugCases {

  private static final Logger logger = Logger.getLogger(TestMainDebugCases.class.getName());

  public TestMainDebugCases() {}

  private boolean runSatSolver(String... args) {
    // run
    Main.main(args);
    return Main.result.toBoolean();
  }

  @Test
  public void testAim50UNSAT2() throws InterruptedException {
    // final String originalPath = "cnf/sat4j_import/aim-50-1_6-no-2.cnf";
    final String originalPath = "cnf/debug/aim-50-1_6-no-2.cnf";
    final String modPath = "cnf/debug/aim-50-1_6-no-2___repeat.cnf";

    foo(originalPath, modPath,
        () -> runSatSolver(modPath, "-initialCrewSize", "256", "-engine", "GPU4"));
  }

  private final static int MAX_ATTEMPTS = 10;

  private void foo(final String originalPath, final String modPath, final Supplier<Boolean> solver) {
    try {
      // read original file into list
      final ArrayList<String> lines = new ArrayList<>();
      final BufferedReader r = new BufferedReader(new FileReader(new File(originalPath)));
      String readline = null;
      while ((readline = r.readLine()) != null) {
        lines.add(readline);
      }
      r.close();

      // try to comment out each line
      int linesRemoved = 0;
      for (int attempt = 0; attempt < MAX_ATTEMPTS; attempt++) {
        for (int i = 0; i < lines.size(); i++) {
          final String line = lines.get(i);
          if (line.startsWith("c") || line.startsWith("p"))
            continue;
          // we have a non-comment line
          // try commenting it
          lines.set(i, "c " + line);

          // write lines to output file
          final BufferedWriter w = new BufferedWriter(new FileWriter(new File(modPath)));
          for (final String s : lines) {
            w.write(s);
            w.newLine();
          }
          w.close();

          // run the solver, hope we still get the assertion
          try {
            final boolean b = solver.get();
          } catch (final AssertionError e) {
            // success! the input is getting smaller.
            // leave this line commented out
            linesRemoved++;
            continue;
          }
          // assertion was not tripped, undo this edit
          lines.set(i, line);
        }
      }

      System.out.println("lines removed by minimzer: " + linesRemoved);

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
