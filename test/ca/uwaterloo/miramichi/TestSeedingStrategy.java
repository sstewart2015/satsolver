package ca.uwaterloo.miramichi;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import ca.uwaterloo.miramichi.seeding.AssumptionSplitterSeedingStrategy;

public class TestSeedingStrategy {

  @Test
  public void test() {
    final AssumptionSplitterSeedingStrategy s = new AssumptionSplitterSeedingStrategy();
    assertTrue(s.hasNextAssumptions());
    final ArrayList<Integer> a1 = s.nextAssumptions();
    assertTrue(a1.isEmpty());
    assertTrue(s.hasNextAssumptions());
    final ArrayList<Integer> a2 = s.nextAssumptions();
    System.out.println(a2);
    assertFalse(a2.isEmpty());
  }

  @Test
  public void test2() {
    final AssumptionSplitterSeedingStrategy s = new AssumptionSplitterSeedingStrategy();
    for (int i = 0; i < 12; i++) {
      s.nextAssumptions();
      // System.out.println(s.nextAssumptions());
    }
  }
}
